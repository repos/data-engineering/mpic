# Metrics Platform Instrument Configurator (MPIC)

Metrics Platform Instrument Configuration web application

## Requirements

* Node.js 20
* Docker + Docker Compose (to be able to run the local test environment)

## Quick start guide

This section shows how to run quickly MPIC locally for development or testing purposes and also how to run the frontend side as a standalone process for debugging purposes

### Run MPIC locally for development purpose with login mechanism disabled

That case MPIC run with a default 

```shell
npm install
cp config.dev.yaml config.local.yaml
<set 'localhost' as the database host (database.host property) at config.local.yaml
<set a database.password property with 'maria' as value at config.local.yaml>
make run-dev
```

>Note that `npm install` is ready to install dependencies for both backend and frontend and `make run-dev` will build the frontend automatically before MPIC starts

### Run MPIC locally for development purpose with login mechanism enabled

- First, you must prepare your environment: [specific instructions for Firefox](#preparing-your-environment-to-make-log-inout-work-locally)
- Then, run:

```shell
npm install
cp config.dev.yaml config.local.yaml
<set 'localhost' as the database host (database.host property) at config.local.yaml>
<set a database.password property with 'maria' as value at config.local.yaml>
<set a service.csrf_secret property with 'secret' as value at config.local.yaml>
<set a service.session_secret property with 'secret' as value at config.local.yaml>
<set the right value for idp.client_secret property at config.local.yaml>
cp frontend/.env.local.sample frontend/.env.local
<ensure that VITE_BASE_URL at frontend/.env.local is equal to '/''>
make run-prod
```

>Note that `npm install` is ready to install dependencies for both backend and frontend and `make run-prod` will build the frontend automatically before MPIC starts

### Run MPIC on a production-like docker environment for testing purporse

- First, you must prepare your environment: [specific instructions for Firefox](#preparing-your-environment-to-make-log-inout-work-locally)
- Then, run: (keep in mind that the database for this environment is not exposing the port to your localhost)

```shell
cp .env.local.sample .env.local
<set the right value for IDP_CLIENT_SECRET at .env.local>
cp frontend/.env.local.sample frontend/.env.local
make startup-qa
```

>Note that the frontend will be built automatically before MPIC starts

### Run MPIC frontend for debugging purposes

In the case you want to run the frontend as a standalone process for frontend debugging purposes:

- Launch the backend in development mode (with the login mechanism disabled)

```shell
npm install
cp config.dev.yaml config.local.yaml
<set 'localhost' as the database host (database.host property) at config.local.yaml>
<set a database.password property with 'maria' as value at config.local.yaml>
make run-dev
```

- And run the frontend as a standalone process (login mechanism will be disable at the backend and you will be running MPIC with a test user so you can access to all the MPIC features):

```shell
cp frontend/.env.local.sample frontend/.env.local (if not done before)
<set 'http://localhost/'' as the value for VITE_BASE_URL at frontend/.env.local>
cd frontend
npm run dev
<go to 'http://localhost:5173' to debug the frontend>
```

### Stoping/Removing the docker environments

Each docker environment can be easily stopped and removed if needed. The service docker image will be also deleted in both cases:

- `make shutdown-dev` to remove the docker development enviroment
- `make shutdown-qa` to remove the docker testing environment

## Getting Started

To develop locally the best idea is to follow the [Quick start guide](#quick-start-guide). There you can find how to run MPIC for development purpose (with/without the login mechanism) and also how to run the frontend to debug there. Anyway note that, by default, the service will look for a `config.dev.yaml` file to load configuration. 

Install the dependencies for backend and frontend (there is a `preinstall` script that runs `npm install` for the frontend automatically). Simply run the following command from the root project folder:

```shell
npm install
```

The frontend can be build for development or production purposes (without or with the login mechanism enabled respectively). This step can be skipped because there is a `prestart` script to do it automatically for development purposes every time we run the service using `npm start` or by using the Makefile goals `make run-dev` and `make run-prod`.

- `development`: Use that way if you want to build the frontend disabling the login mechanism to develop and test the feature you are working on

```shell
npm run build-frontend-dev
```

- `production`: Use that way if you want to build the frontend enabling the login mechanism to fully test the webapp

```shell
npm run build-frontend-prod
```

## Run the service

In general you can run the service as any other NodeJS-Express webapp, just running the main file. In this case, something like this should be enough:

```shell
node server.js
```

That way you will be running MPIC using the default config file which is `config.dev.yaml`. You can also choose which config file you want to use by running, for example, a local one called `config.local.yaml`:

```shell
node server.js --config config.local.yaml
```

Anyway, a few considerations must be taken:
- A database is needed to be able to run MPIC
- Some environment customization might be needed (secrets are provided by environments variables on production-like environments)
- We often don't need login mechanism while working on a different feature and we prefer to disable it

That's why a couple of environments have been created, to run and test the service and the related environment (the database) more easily. The following subsections are about how to run the development (focused on developers who want to work on MPIC) and testing environments (focused on developers/testers who want to test MPIC as on production-like environment). Besides that, you can always go directly to the [Quick start guide](#quick-start-guide) to see how to prepare quickly a proper development environment and run MPIC.

### Running the development environment

MPIC also has a development environment that provides only the database with sample data to use while developing (both test environment and this one can be running at the same time because only this one exposes the database port to localhost). The purpose of this development environment is only to provide a database with some sample data. You, as a developer, can use it and start MPIC locally to test the feature you are working on.

If you want to run the development environment, just run:

```shell
make startup-dev
```

And a docker container will run with the MPIC database with some sample data (A few instruments/experiments already registered). (On Linux you need to either install the appropriate docker-compose shim or just run `docker compose -f docker-compose.dev.yaml up -d`)

In this case, before starting MPIC, you need to provide some needed config properties by adding them to your `config.local.yaml` file (you can also use the `config.dev.yaml` file which is already prepared by just running `cp config.dev.yaml config.local.yaml`):

Anyway, you can always provide them as environment variables when running the command that starts the service.

Once your dev database container has started, you can start MPIC locally both ways:
- `development` mode: You can just run mpic with the login feature disabled with the following command: (Note that the development test environment will start automatically)

```shell
make run-dev
```

- `production` mode: You might want to enable the login mechanism to use it while testing the feature you are working on. You can run MPIC in `production` mode locally with this development environment. In this case be sure you have configured your browser to be able to use mpic through the staging URL following these [specific instructions for Firefox](#preparing-your-environment-to-make-log-inout-work-locally). Once that done, you can run MPIC as follows: (note that the test environment will start automatically)

```shell
make run-prod
```

Keep in mind that, using those ways, you will see a few error message saying that there is no value for the environment variables (if you finally didn't decide to provide their values as environment variables). That's true because you have added those values as config properties in your `config.local.yaml` and not as environment variables. You can ignore them:

```shell
. . .
{"level":"ERROR","time":"2024-05-23T12:58:21.510Z","pid":73378,"hostname":"wmf3277","msg":"Error while setting an env variable. There is no value for DATABASE_PASSWORD"}
{"level":"ERROR","time":"2024-05-23T12:58:21.510Z","pid":73378,"hostname":"wmf3277","msg":"Error while setting an env variable. There is no value for SAL_PASSWORD"}
{"level":"ERROR","time":"2024-05-23T12:58:21.510Z","pid":73378,"hostname":"wmf3277","msg":"Error while setting an env variable. There is no value for IDP_CLIENT_SECRET"}
{"level":"ERROR","time":"2024-06-11T12:40:35.296Z","pid":72079,"hostname":"wmf3277","msg":"Error while setting an env variable. There is no value for SESSION_SECRET"}
{"level":"ERROR","time":"2024-06-11T12:40:35.297Z","pid":72079,"hostname":"wmf3277","msg":"Error while setting an env variable. There is no value for CSRF_SECRET"}
. . .
```

You can stop and destroy the development environment just running:
```shell
make shutdown-dev
```

### Run the frontend as a standalone process

MPIC is composed by a NodeJS backend and a Vue frontend and this one is built static and automatically every time we run MPIC. That way we can treat MPIC as a single web application when running or deploying to staging/production environments. But, sometimes, while working locally on a specific feature for the frontend, we might want to debug something and do that with the static built frontend is really difficult. In this case we need to run the frontend as a standalone process running the following command from the `frontend` folder:

```shell
npm run dev
```

In that case, be also sure that you are using `http://localhost/` as the value for `VITE_BASE_URL` (instead of `/`) at your `frontend/.env.local` file (Now backend and frontend are differente processes).

The backend can be run as explained, using another terminal, at [running the development environment](#running-the-development-environment). For example, in that case, after starting the development environment (`make startup-dev`) you can run mpic with `npm start`. 

Finally you can go to `http://localhost:5173` to use the standalone frontend instead of the one we have built statically running MPIC backend.

That way you are running the backend as usual but the frontend you are debugging is not statically built. Any error/warning or debugging message will be easily understood using the developer console with your browser. Keep also in mind that, by default using this way, MPIC is running in development mode and the login mechanism will be disabled.

### Running the docker testing environment

MPIC has a docker testing environment that runs as a docker compose project with two containers:
- `mpic-test-database`: It's a container that runs a database with the tables that MPIC needs and sample data
- `mpic-test`: It's a container that runs MPIC in production mode ready to be tested as on production-like environment

Before moving on, be sure you have prepared your environment to make log in/out work locally using your browser ([specific instructions are available for Firefox](#preparing-your-environment-to-make-log-inout-work-locally)). The purpose of this testing environment is to test MPIC fully on a production-like environment before deploying a new feature so all the features will be enabled and MPIC will be run as a docker container whose image will be created directly from the `blubber` file as we do at staging/production environments.

If you want to run this testing environment, first you need to create a `.env.local` file to provide values for the secrets that MPIC uses (those values will be use to populate the environment for the MPIC container). The fastest way to do this is to copy the sample one (`.env.local.sample`) and put the right value for the IDP client secret. The rest of the values can be set as they already are:

```shell
cp .env.local.sample .env.local
<set the right value for IDP_CLIENT_SECRET at .env.local>
```

After that you can start this environment by running:

```shell
make startup-qa
```

MPIC will be waiting for you at http://mpic-next.wikimedia.org (the URL will be mapped to `localhost`)

And you can stop and destroy it by running:

```shell
make shutdown-qa
```

#### Run the front end tests

MPIC uses [WebdriverIO](https://webdriver.io/) as the front end testing framework to run e2e tests. If you are running a local development environment with login disabled, you can run the following npm command from the `/frontend` directory to run tests:

```shell
npm run wdio
```

## Preparing your environment to make log in/out work locally

In this section the browser configuration instructions are specific for Firefox but something similar should work for almost any browser.

We are using CAS-SSO for users to log in. That requires some configuration in the CAS-SSO infrastructure in such a way we only can use staging and production URLs while testing these features even locally. If you want to test log in/out features locally, you must follow these steps:

- Be sure you are using `https://idp-test.wikimedia.org/logout` as the value for `VITE_IDP_LOGOUT_URL` at your `frontend/.env.local` file
- Consider Firefox as the browser to test locally (It seems it is the easiest to configure)
- Map `mpic-next.wikimedia.org` to `127.0.0.1` at your `/etc/hosts`
- Just in case, clear DNS Cache
- We need to configure the browser to avoid `https` navigation that modern browsers force automatically:
  - Remove history/cookie/cache/... for `mpic-next.wikimedia.org`
  - Set `network.stricttransportsecurity.preloadlist` to `false` at the `about:config` special page (we need to disable [HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security) at least while testing these features)
  - Restart your browser at this time
- Configure MPIC to run on the port 80 (`service.port` property at your `config.local.yaml` file)
- Start MPIC and, using Firefox, go (using `http` explicitly) to `http://mpic-next.wikimedia.org`
- Once you are at `https://idp-test.wikimedia.org/login` you can use your wmf production credentials to log in

## Available API endpoints

* GET `http://localhost`: You'll see the login page if the login mechanism is enabled or the instrument catalog if disabled
* GET 'http://localhost/api/v1/instruments': Returns an array that contains the JSON representation of all registered instruments
* GET 'http://localhost/api/v1/experiments': Returns an array that contains the JSON representation of all registered A/B tests

## Project structure

- `.pipeline`: Contains the `blubber.yaml` file to generate the Dockerfile for this service
- `config`: Configuration files for service and database
- `controller`: Controller layer
  - `instrumentController.js`: Experiment handling
  - `instrumentController.js`: Instrument handling
  - `locationController.js`: Locations handling using the NOC API
  - `streamController.js`: Streams handling using the Action API
- `db`: Initialization script for database to populate when building the development environment
- `frontend`: Frontend source code
- `public`: Where the frontend is stored after building it statically
- `routes`: Route related files
  - `experiments.js`: Routes to manage experiments
  - `instruments.js`: Routes to manage instruments
  - `locations.js`: Route to manage locations
  - `login.js`: Routes to manage user login/logout
  - `streams.js`: Routes to manage streams
  - `validators.js`: Middleware to validate the request body when registering/modifying instruments
- `service`: Service layer
  - `experimentService.js`: Service layer to manage experiment data
  - `instrumentService.js`: Service layer to manage instrument data
- `test`: Test folder
  - `integration`: Integration tests cases
  - `unit`: Unit tests cases and mock data
- `util`: Util libraries
  - `actionApi.js`: Utility functions to make requests to the Action API
  - `actionLogger.js`: Functions to log "deployment-like" events
  - `auth.js`: Middleware to check if a user is logged in
  - `dateUtil.js`: Utility date functions
- `.env`: Environment properties for docker-compose projects
- `.env.local.sample`: Sample file to create a `.env.local` one
- `.gitlab-ci.yml`: Gitlab pipeline for CI/CD
- `app.js`: Main application file
- `config.dev.yaml`: Configuration file for the development environment
- `config.prod.yaml`: Configuration file for the production environment
- `docker-compose.dev.yaml`: Docker compose file to build a development environment (only the database)
- `docker-compose.yaml`: Docker compose file to build a full environment (MPIC + database)
- `server.js`: Main backend file to launch the service

## Tests

To fire the test suite up (unit and integration test cases), simply run:

```shell
make all-test
```

>A new MPIC development environment will be created automatically with a clean database and the expected sample data that integration tests need to run properly (the environment will be destroyed and created every time).

>Note that logging is disabled when running tests

You can also run only unit or integration tests:

```shell
npm run unit-test
npm run integration-test
```

>Note that integration tests need a real database running with the expected sample data (in that case you will need to run `make startup-dev` first to have it)

### Run unit tests as a production-like docker container

You can also build a docker image and run the unit tests locally with a docker container in a production-like environment:

To build the image:

```shell
docker build --target test -f .pipeline/blubber.yaml -t mpic-test .
```

To run the container for the first time:

```shell
docker run --name mpic-test mpic-test
```

## Troubleshooting

- In a lot of cases, when there is an issue with node, it helps to recreate the
`node_modules` directory:

```shell
rm -rf node_modules
rm -rf frontend/node_modules
fresh-node
npm install
```

- If you think you are experiencing cache issues while running any of the docker enviroments, we recommend to run `docker builder prune` to remove docker cache and try again.

'use strict';

const { app, config, logger } = require('./app');

app.listen(config.service.port, function () {
    logger.info(config.service.name + ' has started listening on port ' + config.service.port);
    logger.info(config.service.name + ' is running on ' + process.env.NODE_ENV + ' environment');
});

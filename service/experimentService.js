'use strict';

const database = require('../config/database');
const { logger } = require('../config/configuration');

// Fetch all experiments with their associated variants
const findAllExperiments = async (columns) => {
    try {
        const experiments = await database.db('instruments').select(columns).where('type', 'A/B test');
        experiments.forEach((item) => {   
            // Reformat variants and output as a "features" property to simplify
            // the api response per T375906.
            const variants = JSON.parse(item.variants)
            item.features = getFeatures(variants);
            // Then remove the variants property to avoid refactoring the instrument store.
            // We can change the api response to simplify the configuration consumed by the
            // Metrics Platform extension to avoid extraneous processing.
            delete item.variants;

            item.sample_rate = JSON.parse(item.sample_rate);
            item.owner = JSON.parse(item.owner);
            item.purpose = JSON.parse(item.purpose);
		    });
        
        return experiments;
    } catch (error) {
        logger.error('Error while fetching experiments: ' + error.stack);
        throw new Error('Error while fetching experiments');
    }
};

function getFeatures(variants) {
    return variants.reduce(
        ( acc, { name, type, values } ) => {
          acc[ name ] = {
            control: values[ 0 ],
            type,
            values
          };

          return acc;
        },
        {}
      );
}

module.exports = {
	findAllExperiments
};

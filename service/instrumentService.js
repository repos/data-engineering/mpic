'use strict';

const database = require('../config/database');
const { logger } = require('../config/configuration');
const { getProgress } = require('../util/dateUtil');

const APP_CONTEXTUAL_ATTRIBUTES = [
	'mediawiki_database',
	'page_id',
	'page_title',
	'page_namespace_id',
	'page_namespace_name,',
	'page_revision_id',
	'page_wikidata_qid',
	'page_content_language',
	'performer_id',
	'performer_name',
	'performer_is_logged_in',
	'performer_is_temp',
	'performer_session_id',
	'performer_pageview_id',
	'performer_groups',
	'performer_language_groups',
	'performer_language_primary',
	'performer_registration_dt'
];

const WEB_CONTEXTUAL_ATTRIBUTES = [
	'page_id',
	'page_title',
	'page_namespace',
	'page_namespace_name',
	'page_revision_id',
	'page_wikidata_qid',
	'page_content_language',
	'page_is_redirect',
	'page_user_groups_allowed_to_move',
	'page_user_groups_allowed_to_edit',
	'mediawiki_skin',
	'mediawiki_version',
	'mediawiki_database',
	'mediawiki_site_content_language',
	'mediawiki_site_content_language_variant',
	'performer_active_browsing_session_token',
	'performer_is_logged_in',
	'performer_id',
	'performer_name',
	'performer_session_id',
	'performer_pageview_id',
	'performer_groups',
	'performer_is_bot',
	'performer_language',
	'performer_language_variant',
	'performer_can_probably_edit_page',
	'performer_edit_count',
	'performer_edit_count_bucket',
	'performer_registration_dt'
];

// Attach contextual attributes to the instrument query result.
const attachInstrumentContextualAttributes = async (contextualAttributes) => {
	const resultMap = await contextualAttributes.reduce((result, row) => {
		result[row.instrument_id] = result[row.instrument_id] || {
		...row,
		contextual_attributes: []
		};
		result[row.instrument_id].contextual_attributes.push(row.contextual_attribute_name);
		return result;
	}, {});

	return resultMap;
};

const fetchContextualAttributes = async (schemaType) => {
	const query = database.db('contextual_attributes').select('*');

	if (schemaType === 'web') {
		query.whereIn('contextual_attribute_name', WEB_CONTEXTUAL_ATTRIBUTES);
	} else if (schemaType === 'app') {
		query.whereIn('contextual_attribute_name', APP_CONTEXTUAL_ATTRIBUTES);
	} 

	try {
		const data =  await query;
		return data;
	} catch (error) {
		logger.error('error while getting contextual attributes: ' + error);
		throw new Error('Error while getting contextual attributes');
	}
};

// Extract contextual attributes details from the instrument data
const extractContextualAttributeKeys = (data) => {
	const contextualAttributesMap = data.contextual_attributes_map;
	let contextualAttributesKeys = Object.keys(contextualAttributesMap);
	contextualAttributesKeys = contextualAttributesKeys.map(Number);
	delete data.contextual_attributes_map;

	return contextualAttributesKeys;
};

// Delete contextual attributes for a specific instrument given its id
const deleteContextualAttributes = async (id, trx) => {
	return await database.db('instrument_contextual_attribute_lookup')
		.where('instrument_id', id)
		.del().transacting(trx);
};

// Relate a specific instrument with a set of contextual attributes
const setContextualAttributes = async (id, contextualAttributesKeys, trx) => {
	const inserts = [];
	contextualAttributesKeys.forEach((value) => {
		const newInsert = {
			instrument_id: id,
			contextual_attribute_id: value
		};
		inserts.push(newInsert);
	});

	return await database.db('instrument_contextual_attribute_lookup')
		.insert(inserts).transacting(trx);
};

const findAllInstruments = async (columns, filterByType = true) => {
    try {
        // Apply the WHERE clause only if filterByType is true
        let query = database.db('instruments').select(columns);
        if (filterByType) {
            query = query.where('type', 'instrument');
        }

        const data = await query.then(async (results) => {
            const contextualAttributes = await database.db('instrument_contextual_attribute_lookup')
                .join(
                    'contextual_attributes',
                    'instrument_contextual_attribute_lookup.contextual_attribute_id',
                    'contextual_attributes.id'
                );
            const resultMap = await attachInstrumentContextualAttributes(contextualAttributes);

            const instruments = [];
            let instrument = null;
            for (const row of results) {
                instrument = row;
                if (resultMap[row.id]) {
                    instrument.contextual_attributes = resultMap[row.id].contextual_attributes;
                } else {
                    instrument.contextual_attributes = [];
                }
                // Calculate and add the progress as a field
                instrument.progress = getProgress(instrument.utc_start_dt, instrument.utc_end_dt);
                instruments.push(instrument);
            }
            return instruments;
        });

        // Formatting sample_rate details as a JSON object
        data.forEach((item) => {
            item.sample_rate = JSON.parse(item.sample_rate);
			item.owner = JSON.parse(item.owner);
			item.purpose = JSON.parse(item.purpose);
        });

        return data;
    } catch (error) {
        logger.error('error while querying the database: ' + error.stack);
        throw new Error('Error while querying the database');
    }
};

const findInstrument = async (givenSlug) => {
	try {
		const data = await database.db('instruments')
			.where({
				slug: givenSlug
			})
			.select('*')
			.then(async (results) => {
				if (!results[0]) {
					return null;
				}

				const contextualAttributes = await database.db('instrument_contextual_attribute_lookup')
					.join(
						'contextual_attributes',
						'instrument_contextual_attribute_lookup.contextual_attribute_id',
						'contextual_attributes.id'
					)
					.where('instrument_contextual_attribute_lookup.instrument_id', results[0].id);
				if (contextualAttributes.length === 0) {
					// Current instrument has no contextual attributes so return original query result.
					return results[0];
				}

				const resultMap = await attachInstrumentContextualAttributes(contextualAttributes);

				const instrument = results[0];
				const contextualAttributesString = resultMap[instrument.id].contextual_attributes.join(',');
				instrument.contextual_attributes = contextualAttributesString;
				return instrument;
			});
		if (!data) {
			return null;
		}

		// Formatting sample_rate details as a JSON object
		data.sample_rate = JSON.parse(data.sample_rate);
		data.variants = JSON.parse(data.variants);
		data.owner = JSON.parse(data.owner);
		data.purpose = JSON.parse(data.purpose);
		// Calculate and add the progress as a field
		data.progress = getProgress(data.utc_start_dt, data.utc_end_dt);
		return data;
	} catch (error) {
		logger.error('error while querying the database: ' + error.stack);
		throw new Error('Error while querying the database');
	}
};

const findExistingInstrument = async (givenName, givenSlug) => {
	try {
		const data = await database.db('instruments')
			.where({
				name: givenName
			})
			.orWhere({
				slug: givenSlug
			})
			.select('id');
		return data;
	} catch (error) {
		logger.error('error while querying the database: ' + error.stack);
		throw new Error('Error while querying the database');
	}
};

const addInstrument = async (data) => {
	try {
		const contextualAttributesKeys = extractContextualAttributeKeys(data);
		data.variants = JSON.stringify(data.variants);
		data.owner = JSON.stringify(data.owner);
		data.purpose = JSON.stringify(data.purpose);

		await database.db.transaction(async (trx) => {
			await database.db('instruments').insert(data).transacting(trx)
			.then(async (instrumentId) => {
				if (instrumentId) {
					if (contextualAttributesKeys.length > 0) {
						const setResponse = await setContextualAttributes(instrumentId, contextualAttributesKeys, trx);
						if (!setResponse) {
							logger.error('contextual attributes settings has failed while registering the instrument');
							throw new Error('contextual attributes settings has failed while registering the instrument');
						}
					}
				} else {
					logger.error('something failed while registering the instrument in the database');
					throw new Error('something failed while registering the instrument in the database');
				}
			});
		});

		return true;
	} catch (error) {
		logger.error('error while registering the instrument: ' + error.stack);
		throw new Error('Error while registering the instrument');
	}
};

const updateInstrument = async (id, data) => {
	try {
		const contextualAttributesKeys = extractContextualAttributeKeys(data);
		data.variants = JSON.stringify(data.variants);
		data.owner = JSON.stringify(data.owner);
		data.purpose = JSON.stringify(data.purpose);

		await database.db.transaction(async (trx) => {
			await database.db('instruments').where('id', id).update(data).transacting(trx)
				.then(async (response) => {
					if (response) {
						await deleteContextualAttributes(id, trx);
						if (contextualAttributesKeys.length > 0) {
							await setContextualAttributes(id, contextualAttributesKeys, trx);
						}
					} else {
						return false;
					}
				});
		});
		return true;
	} catch (error) {
		logger.error('error while updating the instrument: ' + error.stack);
		throw new Error('Error while updating the instrument');
	}
};

const removeInstrument = async (id) => {
	try {
		const response = await database.db.transaction(async (trx) => {
			return await database.db('instruments').where('id', id).del().transacting(trx)
				.then(async (response) => {
					if (response) {
						const deleteResponse = await deleteContextualAttributes(id, trx);
						if (!deleteResponse) {
							logger.warn('There were no contextual attributes related with the deleted instrument');
						}
					}

					return response;
				});
		});

		return response;
	} catch (error) {
		logger.error('error while deleting the instrument: ' + error);
		throw new Error('Error while deleting the instrument');
	}
};

const changeInstrumentStatus = async (id, newStatus) => {
	try {
		const response = await database.db('instruments').where('id', id).update({
			status: newStatus,
			was_activated: newStatus === true ? newStatus : undefined
		});
		return response;
	} catch (error) {
		throw new Error(error);
	}
};

const activateInstrument = async (id) => {
	try {
		const response = await changeInstrumentStatus(id, true);
		return response;
	} catch (error) {
		logger.error('Error while disabling the instrument: ' + error);
		throw new Error('Error while enabling the instrument');
	}
};

const deactivateInstrument = async (id) => {
	try {
		const response = await changeInstrumentStatus(id, false);
		return response;
	} catch (error) {
		logger.error('Error while disabling the instrument: ' + error);
		throw new Error('Error while disabling the instrument');
	}
};

module.exports = {
	findAllInstruments,
	findExistingInstrument,
	findInstrument,
	addInstrument,
	updateInstrument,
	removeInstrument,
	activateInstrument,
	deactivateInstrument,
	fetchContextualAttributes
};

CREATE DATABASE IF NOT EXISTS mpic;

GRANT ALL PRIVILEGES ON mpic.* TO 'maria'@'%';

USE mpic;

CREATE TABLE IF NOT EXISTS instruments (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    slug VARCHAR(255) NOT NULL,
    description TEXT,
    creator VARCHAR(255) NOT NULL,
    owner JSON DEFAULT NULL,
    purpose JSON DEFAULT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    utc_start_dt DATETIME NOT NULL,
    utc_end_dt DATETIME NOT NULL,
    task VARCHAR(1000) NOT NULL,
    compliance_requirements SET('legal', 'gdpr') NOT NULL,
    sample_unit VARCHAR(255) NOT NULL,
    sample_rate JSON NOT NULL,  -- JSON field for sample rates
    environments SET('development', 'staging', 'production', 'external') NOT NULL,
    security_legal_review VARCHAR(1000) NOT NULL,
    status BOOLEAN DEFAULT FALSE,
    was_activated BOOLEAN DEFAULT FALSE,
    stream_name VARCHAR(255),   -- Made optional
    schema_title VARCHAR(255),  -- Made optional
    schema_type VARCHAR(255),   -- Made optional
    email_address VARCHAR(255) NOT NULL,
    type VARCHAR(255) NOT NULL,
    variants JSON DEFAULT NULL, -- New column to store variants as JSON
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS contextual_attributes (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    contextual_attribute_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS instrument_contextual_attribute_lookup (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    instrument_id INT UNSIGNED NOT NULL,
    contextual_attribute_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO contextual_attributes (contextual_attribute_name) VALUES
('page_id'),
('page_title'),
('page_namespace'),
('page_namespace_name'),
('page_revision_id'),
('page_wikidata_id'),
('page_wikidata_qid'),
('page_content_language'),
('page_is_redirect'),
('page_user_groups_allowed_to_move'),
('page_user_groups_allowed_to_edit'),
('mediawiki_skin'),
('mediawiki_version'),
('mediawiki_is_production'),
('mediawiki_is_debug_mode'),
('mediawiki_database'),
('mediawiki_site_content_language'),
('mediawiki_site_content_language_variant'),
('performer_active_browsing_session_token'),
('performer_is_logged_in'),
('performer_id'),
('performer_name'),
('performer_session_id'),
('performer_pageview_id'),
('performer_groups'),
('performer_is_bot'),
('performer_language'),
('performer_language_variant'),
('performer_can_probably_edit_page'),
('performer_edit_count'),
('performer_edit_count_bucket'),
('performer_registration_dt');

INSERT INTO instruments (name, slug, description, creator, owner, purpose, task, compliance_requirements, sample_unit, sample_rate, environments, utc_start_dt, utc_end_dt, security_legal_review, status, stream_name, schema_title, schema_type, email_address, type) VALUES
('test Web Scroll UI', 'test-web-scroll-ui', 'abc description', 'Jane Doe', '["Web Team"]', '["WE.1.1"]', 'https://phabricator.wikimedia.org/T369544', ('legal'), 'pageview', '{"default": 1}', ('development'), '2024-08-30 10:08:53.44', '2024-09-30 10:08:53.44', 'pending', FALSE, 'mediawiki.web_ui_scroll_migrated', 'analytics/product_metrics/web/base', 'web', 'abstract@wikimedia.org', 'instrument'),
('test Desktop UI Interactions', 'test-desktop-ui-interactions', 'efd description', 'Jill Hill', '["Growth Team"]', '["WE.1.2"]', 'https://phabricator.wikimedia.org/T369544', ('gdpr'), 'session', '{"default": 0.5, "0.1": ["frwiki"], "0.01": ["enwiki"]}', ('staging'), '2024-10-08 10:08:53.44', '2024-11-08 10:08:53.44', 'reviewed', FALSE, 'eventlogging_DesktopWebUIActionsTracking', 'analytics/legacy/desktopwebuiactionstracking', 'custom', 'web@wikimedia.org', 'instrument'),
('test Android Article Find In Page', 'test-android-article-find-in-page', 'xyz description', 'Julie Doe', '["Android Team"]', '["WE.1.3"]', 'https://phabricator.wikimedia.org/T369544', ('gdpr,legal'), 'pageview', '{"default": 1.0}', ('production'), '2024-11-08 10:08:53.44', '2024-11-18 10:08:53.44', 'pending', FALSE, 'android.find_in_page_interaction', 'analytics/mobile_apps/android_find_in_page_interaction', 'custom', 'android@wikimedia.org', 'instrument'),
('test Android Article Link Preview Interaction', 'test-android-article-link-preview-interaction', 'www description', 'Jim Doe', '["Android Team"]', '["SDS.2.1"]', 'https://phabricator.wikimedia.org/T369544', ('gdpr,legal'), 'pageview', '{"default": 0.5}', ('production'), '2025-01-08 10:08:53.44', '2025-01-18 10:08:53.44', 'reviewed', FALSE, 'android.product_metrics.article_link_preview_interaction', 'analytics/product_metrics/app/base', 'app', 'android@wikimedia.org', 'instrument'),
('test Android Article TOC', 'test-android-article-toc', 'eee description', 'John Doe', '["Android Team"]', '["SDS.2.2"]', 'https://phabricator.wikimedia.org/T369544', ('legal'), 'pageview', '{"default": 1.0}', ('development'), '2025-01-08 10:08:53.44', '2025-01-30 10:08:53.44', 'reviewd', FALSE, 'android.product_metrics.article_toc_interaction', 'analytics/mobile_apps/product_metrics/android_article_toc_interaction', 'custom', 'android@wikimedia.org', 'instrument');

INSERT INTO instrument_contextual_attribute_lookup (instrument_id, contextual_attribute_id) VALUES
(1, 2),
(1, 3),
(2, 2),
(2, 1),
(3, 1),
(4, 6),
(4, 8),
(4, 10),
(4, 12),
(4, 14),
(4, 15),
(5, 16),
(5, 1),
(5, 2),
(5, 3);


INSERT INTO instruments (name, slug, description, creator, owner, purpose, task, compliance_requirements, sample_unit, sample_rate, environments, utc_start_dt, utc_end_dt, security_legal_review, status, email_address, type, variants) VALUES
('test Web Scroll UI exp', 'test-web-scroll-ui-exp', 'abc description', 'Jane Doe', '["Web Team"]', '["WE.1.1"]', 'https://phabricator.wikimedia.org/T369544', 'legal', 'pageview', '{"default": 1}', 'development', '2025-01-08 10:08:53.44', '2025-01-18 10:08:53.44', 'pending', FALSE, 'abstract@wikimedia.org', 'A/B test','[{"name":"homepage_module","type":"boolean","values":[true, false]}]'),
('test Desktop UI Interactions exp', 'test-desktop-ui-interactions-exp', 'efd description', 'Jill Hill', '["Growth Team"]', '["WE.1.2"]', 'https://phabricator.wikimedia.org/T369544', 'gdpr', 'session', '{"default": 0.5, "0.1": ["frwiki"], "0.01": ["enwiki"]}', 'staging', '2025-01-18 10:08:53.44', '2025-01-30 10:08:53.44', 'reviewed', FALSE, 'web@wikimedia.org', 'A/B test','[{"name":"sticky_header","type":"boolean","values":[true, false]}]'),
('test Android Article Find In Page exp', 'test-android-article-find-in-page-exp', 'xyz description', 'Julie Doe', '["Android Team"]', '["WE.1.3"]', 'https://phabricator.wikimedia.org/T369544', 'gdpr,legal', 'pageview', '{"default": 1.0}', 'production', '2025-02-08 10:08:53.44', '2025-02-18 10:08:53.44', 'pending', FALSE, 'android@wikimedia.org', 'A/B test','[{"name":"table_of_contents","type":"boolean","values":[true, false]}]'),
('test Android Article Link Preview Interaction exp', 'test-android-article-link-preview-interaction-exp', 'www description', 'Jim Doe', '["Android Team"]', '["SDS.2.1"]', 'https://phabricator.wikimedia.org/T369544', 'gdpr,legal', 'pageview', '{"default": 0.5}', 'production', '2025-01-01 10:08:53.44', '2025-01-08 10:08:53.44', 'reviewed', FALSE, 'android@wikimedia.org', 'A/B test','[{"name":"dark_mode","type":"boolean","values":[true, false]}]'),
('test Android Article TOC exp', 'test-android-article-toc-exp', 'eee description', 'John Doe', '["Android Team"]', '["SDS.2.2"]', 'https://phabricator.wikimedia.org/T369544', 'legal', 'pageview', '{"default": 1.0}', 'development', '2025-03-08 10:08:53.44', '2025-03-18 10:08:53.44', 'reviewed', FALSE, 'android@wikimedia.org', 'A/B test','[{"name":"show_preferences","type":"boolean","values":[true, false]}]'),
('test multiple variants exp', 'test-multiple-variants-exp', 'abc description', 'Jane Doe', '["Web Team"]', '["WE.1.1"]', 'https://phabricator.wikimedia.org/T369544', 'legal', 'pageview', '{"default": 1}', 'development', '2025-03-30 10:08:53.44', '2025-01-18 10:08:53.44', 'pending', FALSE, 'abstract@wikimedia.org', 'A/B test','[{"name":"scroll_feature","type":"boolean","values":[true, false]}, {"name":"enable_grid","type":"boolean","values":[true, false]}]');

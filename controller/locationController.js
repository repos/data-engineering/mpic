'use strict';

const { findLocations } = require('../util/actionApi');

const getLocations = (async (req, res) => {
	try {
		const data = await findLocations();
		req.app.logger.info('Locations retrieved successfully using the Action API');
		res.setHeader('cache-control', 'private');
		res.send(data);
	} catch (error) {
		req.app.logger.error('Error while retrieving locations using the Action API: ' + error.stack);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: 'Error while retrieving locations using the Action API'
		});
	}
});

module.exports = {
	getLocations
};

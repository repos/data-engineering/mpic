'use strict';

const _ = require('lodash');

const { findAllExperiments } = require('../service/experimentService');

const getExperiments = async (req, res) => {
	try {
		let columns = ['id', 'name', 'slug', 'description', 'creator', 'owner', 'purpose', 'created_at', 'updated_at',
			'utc_start_dt', 'utc_end_dt', 'task', 'compliance_requirements', 'sample_unit', 'sample_rate', 'environments', 'security_legal_review',
			'status', 'was_activated', 'email_address', 'variants'];
		if (req.path !== '/api/v1/experiments') {
			columns = [...columns, ...['stream_name', 'schema_title', 'schema_type', 'type']];
		}
		const data = await findAllExperiments(columns);
		req.app.logger.info('Experiments retrieved successfully from the database');
		res.setHeader('cache-control', 'private');
		res.send(data);
	} catch (error) {
		req.app.logger.error('Error while retrieving experiments from the database: ' + error);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: 'Error while retrieving experiments'
		});
	}
};

module.exports = {
    getExperiments
};
'use strict';

const { findStreams } = require('../util/actionApi');

const getStreams = (async (req, res) => {
	try {
		const data = await findStreams();
		req.app.logger.info('Streams retrieved successfully using the Action API');
		res.setHeader('cache-control', 'private');
		res.send(data);
	} catch (error) {
		req.app.logger.error('Error while retrieving streams using the Action API: ' + error.stack);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: 'Error while retrieving streams using the Action API'
		});
	}
});

module.exports = {
	getStreams
};

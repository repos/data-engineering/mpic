'use strict';

const _ = require('lodash');
const { validationResult } = require('express-validator');

const { logAction } = require('../util/actionLogger');
const { getEndDate } = require('../util/dateUtil');
const { findAllInstruments, findInstrument, addInstrument, updateInstrument, findExistingInstrument,
	fetchContextualAttributes, removeInstrument, activateInstrument, deactivateInstrument } = require('../service/instrumentService');

const PUBLISH_COLUMNS = [
	'name',
	'slug',
	'description',
	'creator',
	'owner',
	'purpose',
	'created_at',
	'updated_at',
	'utc_start_dt',
	'utc_end_dt',
	'task',
	'compliance_requirements',
	'sample_unit',
	'sample_rate',
	'environments',
	'security_legal_review',
	'status',
	'stream_name',
	'schema_title',
	'schema_type',
	'email_address',
	'type'
];
const UPDATE_COLUMNS = [
	'name',
	'slug',
	'description',
	'owner',
	'purpose',
	'task',
	'updated_at',
	'utc_start_dt',
	'utc_end_dt',
	'compliance_requirements',
	'sample_unit',
	'sample_rate',
	'environments',
	'security_legal_review',
	'status',
	'stream_name',
	'schema_title',
	'schema_type',
	'email_address',
	'type'
];

const getInstruments = (async (req, res) => {
	let filterByType = true; // Default to filtering by type
	let columns = ['id', 'name', 'slug', 'description', 'creator', 'owner', 'purpose', 'created_at', 'updated_at',
		'utc_start_dt', 'utc_end_dt', 'task', 'compliance_requirements', 'sample_unit', 'sample_rate', 'environments', 'security_legal_review',
		'status', 'was_activated', 'stream_name', 'schema_title', 'schema_type', 'email_address'];

	// Not all fields are needed in the response when requesting '/api/v1/instruments'
	if (req.path !== '/api/v1/instruments') {
		// 'progress' column will be added by findAllInstruments below because it's calculated there
		columns = [...columns, ...['type']];
		filterByType = false;
	}

	try {
		// findAllInstruments also calculates and adds the progress for every instrument
		const data = await findAllInstruments(columns,filterByType);
		req.app.logger.info('Instruments retrieved successfully from the database');
		res.setHeader('cache-control', 'private');
		res.send(data);
	} catch (error) {
		req.app.logger.error('Error while retrieving instruments from the database: ' + error);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: 'Error while retrieving instruments'
		});
	}
});

const getInstrument = (async (req, res) => {
	// slug param validation
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		req.app.logger.error('There are some errors while validating the request: ' + JSON.stringify(errors.array()));
		return res.status(400).json({
			status: 400,
			type: 'bad-request',
			title: 'There are some errors while validating the request (invalid slug)',
			detail: errors.array()
		});
	}

	try {
		// findAllInstruments also calculates and adds the progress for the instrument
		const data = await findInstrument(req.params.slug);
		if (data !== null) {
			req.app.logger.info('Instrument ' + req.params.slug + ' retrieved successfully from the database');
			res.setHeader('cache-control', 'private');
			res.status(200).json(data);
		} else {
			const errorMessage = 'Instrument ' + req.params.slug + ' not found';
			req.app.logger.warn(errorMessage);
			res.status(404).json({
				status: 404,
				type: 'not-found',
				title: errorMessage
			});
		}
	} catch (error) {
		const errorMessage = 'Error while fetching an instrument (' + req.params.slug + ')';
			req.app.logger.error(errorMessage + ': ' + error);
			res.status(500).json({
				status: '500',
				type: 'internal-server-error',
				title: errorMessage
			});
	}
});

function decodeJson(quotedString) {
	return quotedString.replace(/&quot;/g, '"');
}

const postInstrument = (async (req, res) => {
	// TODO: is there a better way to post/put json objects?
	req.body.sample_rate = decodeJson(req.body.sample_rate);

	// Form validation
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		req.app.logger.error('There are some errors while validating the form: ' + JSON.stringify(errors.array()));
		return res.status(400).json({
			status: 400,
			type: 'bad-request',
			title: 'There are some errors while validating the form',
			detail: errors.array()
		});
	}

	const createdAt = new Date();
	const updatedAt = new Date();

	// Register instrument
	const data = Object.assign(_.pick(req.body, PUBLISH_COLUMNS), {
		creator: req.user.id,
		created_at: createdAt,
		updated_at: updatedAt,
		contextual_attributes_map: req.body.contextual_attributes_map,
		variants: req.body.variants
	});

	try {
		req.app.logger.info(data);
		// First, look for an existing instrument with the same name or slug
		const existingInstrument = await findExistingInstrument(data.name, data.slug);
		if (existingInstrument.length === 0) {
			// There is no an existing instrument with the same name, the new one is registered
			const result = await addInstrument(data);
			if (result) {
				// If activated, log an entry to SAL
				if (req.app.conf.sal) {
					logAction(req, data.name, data.creator, 'registered');
				} else {
					req.app.logger.warn('Skipping logging to SAL');
				}

				req.app.logger.info('Instrument registered successfully in the database');
				res.status(201).json({
					status: 201,
					type: 'created',
					title: 'Instrument registered successfully'
				});
			}
		} else {
			req.app.logger.warn('There is already an instrument with the same name/slug');
			res.status(409).json({
				status: 409,
				type: 'conflict',
				title: 'There is already an instrument with the same name/slug'
			});
		}
	} catch (error) {
		req.app.logger.error('Error while registering the instrument: ' + error);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: 'Error while registering the instrument'
		});
	}
});

const putInstrument = (async (req, res) => {
	req.body.sample_rate = decodeJson(req.body.sample_rate);

	// Form validation
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		req.app.logger.error('There are some errors while validating the form: ' + JSON.stringify(errors.array()));
		return res.status(400).json({
			status: 400,
			type: 'bad-request',
			title: 'There are some errors while validating the form',
			detail: errors.array()
		});
	}

	// Update the instrument
	const data = Object.assign(_.pick(req.body, UPDATE_COLUMNS), {
		updated_at: new Date(),
		contextual_attributes_map: req.body.contextual_attributes_map,
		variants: req.body.variants
	});

	try {
		req.app.logger.info(data);
		const result = await updateInstrument(req.body.id, data);
		if (result) {
			// If activated, log an entry to SAL
			if (req.app.conf.sal) {
				logAction(req, data.name, data.creator, 'updated');
			} else {
				req.app.logger.warn('Skipping logging to SAL');
			}

			req.app.logger.info('Instrument updated successfully in the database');
			res.status(200).json({
				status: 200,
				type: 'ok',
				title: 'Instrument updated successfully'
			});
		} else {
			req.app.logger.info('Instrument not found');
			res.status(404).json({
				status: 404,
				type: 'not-found',
				title: 'Instrument not found'
			});
		}
	} catch (error) {
		req.app.logger.error('Error while updating the instrument: ' + error);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: error.toString()
		});
	}
});

const deleteInstrument = (async (req, res) => {
	try {
		const result = await removeInstrument(req.params.id);
		if (result) {
			// If activated, log an entry to SAL
			if (req.app.conf.sal) {
				logAction(req, 'instrument with id ', req.params.id, 'deleted');
			} else {
				req.app.logger.warn('Skipping logging to SAL');
			}

			req.app.logger.info('Instrument successfully deleted from the database');
			res.status(200).json({
				status: 200,
				type: 'ok',
				title: 'Instrument deleted successfully'
			});
		} else {
			req.app.logger.error('Instrument not found');
			res.status(404).json({
				status: 404,
				type: 'not-found',
				title: 'Instrument not found'
			});
		}
	} catch (error) {
		req.app.logger.error('Error while deleting instrument: ' + error);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: error.toString()
		});
	}
});

const patchInstrumentStatus = (async (req, res) => {
	try {
		let result = null;
		let action = null;
		if (req.body.status === true) {
			action = 'activated';
			result = await activateInstrument(req.params.id);
		} else {
			action = 'deactivated';
			result = await deactivateInstrument(req.params.id);
		}

		if (result) {
			// If activated, log an entry to SAL
			if (req.app.conf.sal) {
				logAction(req, req.body.name, req.user.id, action);
			} else {
				req.app.logger.warn('Skipping logging to SAL');
			}

			req.app.logger.info('Instrument successfully ' + action);
			res.status(200).json({
				status: 200,
				type: 'ok',
				title: 'Instrument ' + action + ' successfully'
			});
		} else {
			req.app.logger.info('Instrument not found');
			res.status(404).json({
				status: 404,
				type: 'not-found',
				title: 'Instrument not found'
			});
		}
	} catch (error) {
		req.app.logger.error(error);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: error.toString()
		});
	}
});

const getContextualAttributes = (async (req, res) => {
	try {
		const data = await fetchContextualAttributes(req.query['schema-type']);
		if (data.length) {
			req.app.logger.info('Contextual attributes retrieved successfully from the database');
			res.setHeader('cache-control', 'private');
			res.status(200).json(data);
		} else {
			const errorMessage = 'Contextual attributes not found';
			req.app.logger.warn(errorMessage);
			res.status(404).json({
				status: 404,
				type: 'not-found',
				title: errorMessage
			});
		}
	} catch (error) {
		req.app.logger.error('Error while getting contextual attributes: ' + error);
		res.status(500).json({
			status: 500,
			type: 'internal-server-error',
			title: 'Error while getting contextual attributes: ' + error
		});
	}
});

module.exports = {
	postInstrument,
	getInstrument,
	getInstruments,
	putInstrument,
	deleteInstrument,
	patchInstrumentStatus,
	getContextualAttributes
};

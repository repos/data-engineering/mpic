'use strict';

const express = require('express');
const router = express.Router();

const { isAuth } = require('../util/auth');
const { getLocations } = require('../controller/locationController');

router.get('/locations', isAuth, getLocations);

module.exports = router;

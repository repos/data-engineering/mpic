'use strict';

const express = require('express');
const router = express.Router();
const { Issuer, Strategy } = require('openid-client');
const passport = require('passport');
const { config } = require('../config/configuration');
const { getUserGroups, getCsrfToken } = require('../util/auth');

Issuer.discover(config.idp.oidc_url)
    .then(function (oidcIssuer) {
        const client = new oidcIssuer.Client({
            client_id: config.idp.client_id,
            client_secret: config.idp.client_secret,
            redirect_uris: [config.idp.redirect_uri],
            response_types: [config.idp.response_type]
        });

        passport.use('oidc', new Strategy({ client, passReqToCallback: true }, (req, tokenSet, userinfo, done) => {
            return done(null, userinfo);
        }));
    });

passport.serializeUser((user, next) => {
    next(null, {
        id: user.id,
        roles: getUserGroups(user.memberOf)
    });
});
passport.deserializeUser((user, next) => {
    next(null, user);
});

router.get('/login', passport.authenticate('oidc', { scope: 'openid groups' }));

router.get('/logout', function (req, res, next) {
    req.session.destroy((err) => {
        if (!err) {
            req.app.logger.debug('Logout successful');
            res.status(200).json({});
        } else {
            req.app.logger.debug('Logout failed');
            res.status(500).json({});
        }
    });
});

router.get('/login/callback',
    passport.authenticate('oidc', { failureRedirect: '/' }),
        async (req, res) => {
            // Generate the CSRF Token (once per session)
            const csrfToken = getCsrfToken(req);
            // Set a cookie with the CSRF Token and also in the session to check when processing the POST request
            res.cookie('csrf_token', csrfToken);
            req.session.csrfToken = csrfToken;

            res.redirect('/');
        }
);

router.get('/user', function (req, res) {
    if (!req.user) {
        res.status(404).json({
            status: '404',
            type: 'not-found',
            title: 'not-found'
        });
    } else {
        res.setHeader('cache-control', 'no-cache, no-store, must-revalidate');
        res.setHeader('Pragma', 'no-cache');
        res.setHeader('Expires', 0);
        res.status(200).json(req.user);
    }
});

module.exports = router;

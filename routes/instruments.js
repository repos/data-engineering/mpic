'use strict';

const express = require('express');
const router = express.Router();

const { postInstrument, getInstrument, getInstruments, putInstrument, deleteInstrument,
    patchInstrumentStatus, getContextualAttributes } = require('../controller/instrumentController');
const { instrumentBodyValidator, instrumentSlugParamValidator,
    patchInstrumentStatusValidator } = require('./validators');
const { isAuth, validateCsrfToken } = require('../util/auth');

// No auth required for this endpoint
router.get('/api/v1/instruments', getInstruments);
router.get('/instruments', isAuth, getInstruments);
router.get('/instrument/:slug', isAuth, instrumentSlugParamValidator, getInstrument);
router.put('/instrument/:slug', isAuth, validateCsrfToken, instrumentBodyValidator, putInstrument);
router.post('/instruments', isAuth, validateCsrfToken, instrumentBodyValidator, postInstrument);
router.delete('/instrument/:id', isAuth, validateCsrfToken, instrumentSlugParamValidator, deleteInstrument);
router.patch('/instrument/:id', isAuth, validateCsrfToken, patchInstrumentStatusValidator, patchInstrumentStatus);
router.get('/contextual-attributes', isAuth, getContextualAttributes);

module.exports = router;

'use strict';

const express = require('express');
const router = express.Router();

const { isAuth } = require('../util/auth');
const { getStreams } = require('../controller/streamController');

router.get('/streams', isAuth, getStreams);

module.exports = router;

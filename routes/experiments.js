'use strict';

const express = require('express');
const router = express.Router();

const { postInstrument, putInstrument } = require('../controller/instrumentController');
const { experimentBodyValidator } = require('./validators');
const { isAuth, validateCsrfToken } = require('../util/auth');
const { getExperiments } = require('../controller/experimentController');

router.post('/experiments', isAuth, validateCsrfToken, experimentBodyValidator, postInstrument);
router.put('/experiment/:slug', isAuth, validateCsrfToken, experimentBodyValidator, putInstrument);
router.get('/api/v1/experiments', getExperiments);
router.get('/experiments', isAuth, getExperiments);

module.exports = router;

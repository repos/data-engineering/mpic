'use strict';

const { body, param } = require('express-validator');

const instrumentSlugParamValidator = [
    param('slug').notEmpty().trim().escape().matches(/^[a-zA-Z0-9-]+$/)
  ];

const patchInstrumentStatusValidator = [
    body('name').notEmpty().trim().matches(/^[a-zA-Z0-9 ]+$/).withMessage('Name must contain only letters, numbers, and spaces'),
];

const instrumentBodyValidator = [
    body('name', 'cannot be empty').notEmpty().trim().bail()
        .matches(/^[a-zA-Z0-9 ]+$/).withMessage('Name must contain only letters, numbers, and spaces'),
    body('description').trim(),
    body('creator').trim(),
    body('owner').isArray(),
    body('purpose').isArray(),
    body('task').notEmpty().trim().withMessage('cannot be empty'),
    body('utc_start_dt').notEmpty().trim().withMessage('start date cannot be empty'),
    body('utc_end_dt').notEmpty().trim().withMessage('end date cannot be empty'),
    body('compliance_requirements').notEmpty().trim().withMessage('cannot be empty'),
    body('sample_unit').notEmpty().trim().withMessage('cannot be empty'),
    body('sample_rate').notEmpty().trim(),
    body('status').notEmpty().trim(),
    body('security_legal_review').notEmpty().trim(),
    body('stream_name').notEmpty().trim().withMessage('cannot be empty'),
    body('schema_title').notEmpty().trim().withMessage('cannot be empty'),
    body('schema_type').notEmpty().trim().withMessage('cannot be empty'),
    body('email_address').notEmpty().trim().withMessage('cannot be empty'),
    body('type').notEmpty().trim().equals('instrument')
        .withMessage('Type must be instrument'),
    body('acknowledgement', 'You must confirm that you have followed the data collection guidelines').equals('true')
];

const experimentBodyValidator = [
    body('name', 'cannot be empty').notEmpty().trim().bail()
        .matches(/^[a-zA-Z0-9 ]+$/).withMessage('Name must contain only letters, numbers, and spaces'),
    body('description').trim(),
    body('creator').trim(),
    body('owner').isArray(),
    body('purpose').isArray(),
    body('task').notEmpty().trim().withMessage('cannot be empty'),
    body('utc_start_dt', 'start date cannot be empty').notEmpty().trim().withMessage('start date cannot be empty'),
    body('utc_end_dt').notEmpty().trim().withMessage('end date cannot be empty'),
    body('compliance_requirements').notEmpty().trim().withMessage('cannot be empty'),
    body('sample_unit').notEmpty().trim().withMessage('cannot be empty'),
    body('sample_rate').notEmpty().trim(),
    body('status').notEmpty().trim(),
    body('security_legal_review').notEmpty().trim().withMessage('cannot be empty'),
    body('email_address').notEmpty().trim().withMessage('cannot be empty'),
    body('type').equals('A/B test'),
    body('variants').isArray({min: 1}).withMessage('cannot be empty'),
    body('variants.*.name').notEmpty().trim(),
    body('variants.*.type').equals('boolean'),
    body('variants.*.values').isArray({ min: 2, max: 2}).isIn(['true', 'false']),
    body('acknowledgement', 'You must confirm that you have followed the data collection guidelines').equals('true')
];

module.exports = {
    instrumentBodyValidator,
    instrumentSlugParamValidator,
    patchInstrumentStatusValidator,
    experimentBodyValidator
};

'use strict';

const expect = require('chai').expect;
const { getProgress } = require('../../util/dateUtil');

beforeAll(() => {
    jest.useFakeTimers();
    // Fake time is 2024-01-01
    jest.setSystemTime(new Date(2024, 0, 1));
});

afterAll(() => {
    jest.useRealTimers();
});

describe('dateUtil', () => {
    it ('progress is well calculated', () => {
        let startDate = new Date('2024-01-01');
        let endDate = new Date('2024-01-03')
        let actualProgress = getProgress(startDate, endDate);
        let expectedProgress = 0;
        expect(actualProgress).equal(expectedProgress);

        startDate = new Date('2023-12-25');
        endDate = new Date('2024-01-08')
        actualProgress = getProgress(startDate, endDate);
        expectedProgress = 0.5;
        expect(actualProgress).equal(expectedProgress);

        startDate = new Date('2023-12-25');
        endDate = new Date('2024-01-01')
        actualProgress = getProgress(startDate, endDate);
        expectedProgress = 1;
        expect(actualProgress).equal(expectedProgress);

        startDate = new Date('2023-12-31');
        endDate = new Date('2024-01-10')
        actualProgress = getProgress(startDate, endDate);
        expectedProgress = 0.1;
        expect(actualProgress).equal(expectedProgress);
    });
});

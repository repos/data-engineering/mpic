'use strict';

const httpMocks = require('node-mocks-http');
const { describe, it, expect, afterEach } = require('@jest/globals');
const testUtil = require('../testUtil');

jest.mock('../../service/experimentService');

const experimentController = require('../../controller/experimentController');
const experimentService = require('../../service/experimentService');
const { mockApiExperimentsArray } = require('./mocks/experiments');

const mockFindAllExperiments = jest.spyOn(experimentService, 'findAllExperiments');

afterEach(() => {
    jest.clearAllMocks();
});


describe('Experiment controller', () => {
    it('GET /api/v1/experiments should get experiment list', async () => {
        const response = httpMocks.createResponse({
            eventEmitter: require('events').EventEmitter
        });
        const request = testUtil.prepareRequest();
        request.path = '/api/v1/experiments';

        const mockExperimentList = jest.fn(async () => {
            return mockApiExperimentsArray;
        });
        mockFindAllExperiments.mockImplementation(mockExperimentList);

        await experimentController.getExperiments(request, response);

        // Listen for the 'end' event to ensure the response is complete
        response.on('end', () => {
            const responseData = response._getData();
            console.log('Response data:', responseData);

            expect(mockFindAllExperiments).toHaveBeenCalledTimes(1);
            expect(response.statusCode).toEqual(200);
            expect(response._isEndCalled()).toBeTruthy();
            expect(responseData.length).toEqual(5); // Expect 5 experiments in the response
        });

        response.end(); 
    });
});

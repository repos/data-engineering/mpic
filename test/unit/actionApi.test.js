'use strict';

const expect = require('chai').expect;
const { rawMockStreams, parsedMockStreams, rawMockLocations, parsedMockLocations } = require('./mocks/actionApi');
const { parseStreams, parseLocations } = require('../../util/actionApi');

describe('actionAPI', () => {
    it('parseStreams', () => {
        const parsedActualStreams = parseStreams(rawMockStreams);
        expect(parsedActualStreams).to.eql(parsedMockStreams);
    });

    it('parseLocations', () => {
        const parsedActualLocations = parseLocations(rawMockLocations);
        expect(parsedActualLocations).to.eql(parsedMockLocations);
    });
});

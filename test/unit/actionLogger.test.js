'use strict';

const expect = require('chai').expect;
const actionLogger = require('../../util/actionLogger');

describe('actionLogger', () => {
    it('A new action log entry is generated properly', () => {
        const now = new Date();
        const expectedValue = '\n* ' + now.getHours().toString().padStart(2, '0') + ':' + now.getMinutes().toString().padStart(2, '0') +
        ' the creator: A new instrument has been created - the-instrument-name';
        const actualValue = actionLogger.getActionLoggerEntry('the creator', 'the-instrument-name', 'created');

        expect(actualValue).equal(expectedValue);
    });
});

'use strict';

exports.mockApiExperimentArray = [
  {
    "id": 6,
    "name": "test Web Scroll UI exp",
    "slug": "test-web-scroll-ui-exp",
    "description": "abc description",
    "creator": "Jane Doe",
    "owner": ["Web Team"],
    "purpose": ["FY24/25 WE.1.1"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-03-01T00:00:00.000Z",
    "utc_end_dt": "2024-03-08T00:00:00.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "legal",
    "sample_unit": "pageview",
    "sample_rate": "{\"default\": 1}",
    "environments": "development",
    "security_legal_review": "pending",
    "status": 0,
    "email_address": "abstract@wikimedia.org",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  },
  {
    "id": 7,
    "name": "test Desktop UI Interactions exp",
    "slug": "test-desktop-ui-interactions-exp",
    "description": "efd description",
    "creator": "Jill Hill",
    "owner": ["Growth Team"],
    "purpose": ["FY24/25 WE.1.2"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-05-01T00:00:00.000Z",
    "utc_end_dt": "2024-05-15T00:00:00.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "gdpr",
    "sample_unit": "session",
    "sample_rate": "{\"default\": 0.5, \"0.1\": [\"frwiki\"], \"0.01\": [\"enwiki\"]}",
    "environments": "staging",
    "security_legal_review": "reviewed",
    "status": 0,
    "email_address": "web@wikimedia.org",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  },
  {
    "id": 8,
    "name": "test Android Article Find In Page exp",
    "slug": "test-android-article-find-in-page-exp",
    "description": "xyz description",
    "creator": "Julie Doe",
    "owner": ["Android Team"],
    "purpose": ["FY24/25 WE.1.3"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-05-20T00:00:00.000Z",
    "utc_end_dt": "2024-06-01T00:00:00.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "legal,gdpr",
    "sample_unit": "pageview",
    "sample_rate": "{\"default\": 1.0}",
    "environments": "production",
    "security_legal_review": "pending",
    "status": 0,
    "email_address": "android@wikimedia.org",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  },
  {
    "id": 9,
    "name": "test Android Article Link Preview Interaction exp",
    "slug": "test-android-article-link-preview-interaction-exp",
    "description": "www description",
    "creator": "Jim Doe",
    "owner": ["Android Team"],
    "purpose": ["FY24/25 SDS.2.1"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-04-25T00:00:00.000Z",
    "utc_end_dt": "2024-05-10T00:00:00.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "legal,gdpr",
    "sample_unit": "pageview",
    "sample_rate": "{\"default\": 0.5}",
    "environments": "production",
    "security_legal_review": "reviewed",
    "status": 0,
    "email_address": "android@wikimedia.org",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  },
  {
    "id": 10,
    "name": "test Android Article TOC exp",
    "slug": "test-android-article-toc-exp",
    "description": "eee description",
    "creator": "John Doe",
    "owner": ["Android Team"],
    "purpose": ["FY24/25 SDS.2.2"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-05-15T00:00:00.000Z",
    "utc_end_dt": "2024-06-10T00:00:00.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "legal",
    "sample_unit": "pageview",
    "sample_rate": "{\"default\": 1.0}",
    "environments": "development",
    "security_legal_review": "reviewed",
    "status": 0,
    "email_address": "android@wikimedia.org",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  }
];

exports.mockExperimentArray = [
  {
    "id": 6,
    "name": "test Web Scroll UI exp",
    "slug": "test-web-scroll-ui-exp",
    "description": "abc description",
    "creator": "Jane Doe",
    "owner": ["Web Team"],
    "purpose": ["FY24/25 WE.1.1"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-03-01T00:00:00.000Z",
    "utc_end_dt": "2024-03-08T00:00:00.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "legal",
    "sample_unit": "pageview",
    "sample_rate": "{\"default\": 1}",
    "environments": "development",
    "security_legal_review": "pending",
    "status": 0,
    "email_address": "abstract@wikimedia.org",
    "type": "A/B test",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  },
  {
    "id": 7,
    "name": "test Desktop UI Interactions exp",
    "slug": "test-desktop-ui-interactions-exp",
    "description": "efd description",
    "creator": "Jill Hill",
    "owner": ["Growth Team"],
    "purpose": ["FY24/25 WE.1.2"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-05-01T00:00:00.000Z",
    "utc_end_dt": "2024-05-15T00:00:00.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "gdpr",
    "sample_unit": "session",
    "sample_rate": "{\"default\": 0.5, \"0.1\": [\"frwiki\"], \"0.01\": [\"enwiki\"]}",
    "environments": "staging",
    "security_legal_review": "reviewed",
    "status": 0,
    "email_address": "web@wikimedia.org",
    "type": "A/B test",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  },
  {
    "id": 8,
    "name": "test Android Article Find In Page exp",
    "slug": "test-android-article-find-in-page-exp",
    "description": "xyz description",
    "creator": "Julie Doe",
    "owner": ["Android Team"],
    "purpose": ["FY24/25 WE.1.3"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-05-20T00:00:00.000Z",
    "utc_end_dt": "2024-06-01T00:00:00.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "legal,gdpr",
    "sample_unit": "pageview",
    "sample_rate": "{\"default\": 1.0}",
    "environments": "production",
    "security_legal_review": "pending",
    "status": 0,
    "email_address": "android@wikimedia.org",
    "type": "A/B test",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  },
  {
    "id": 9,
    "name": "test Android Article Link Preview Interaction exp",
    "slug": "test-android-article-link-preview-interaction-exp",
    "description": "www description",
    "creator": "Jim Doe",
    "owner": ["Android Team"],
    "purpose": ["FY24/25 SDS.2.1"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-06-01 23:05:14",
    "utc_end_dt": "2024-06-30 23:01:08",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "legal,gdpr",
    "sample_unit": "pageview",
    "sample_rate": "{\"default\": 0.5}",
    "environments": "production",
    "security_legal_review": "reviewed",
    "status": 0,
    "email_address": "android@wikimedia.org",
    "type": "A/B test",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  },
  {
    "id": 10,
    "name": "test Android Article TOC exp",
    "slug": "test-android-article-toc-exp",
    "description": "eee description",
    "creator": "John Doe",
    "owner": ["Android Team"],
    "purpose": ["FY24/25 SDS.2.2"],
    "created_at": "2024-09-17T11:57:10.000Z",
    "updated_at": "2024-09-17T11:57:10.000Z",
    "utc_start_dt": "2024-09-01T23:05:14.000Z",
    "utc_end_dt": "2024-09-30T23:01:08.000Z",
    "task": "https://phabricator.wikimedia.org/T369544",
    "compliance_requirements": "legal",
    "sample_unit": "pageview",
    "sample_rate": "{\"default\": 1.0}",
    "environments": "development",
    "security_legal_review": "reviewed",
    "status": 0,
    "email_address": "android@wikimedia.org",
    "type": "A/B test",
    "variants": [
      {
        "name": "homepage_module",
        "type": "boolean",
        "values": [true, false]
      }
    ]
  }
];
exports.mockExperiment = {
  id: 11,
  name: 'Mock Experiment',
  slug: 'mock-experiment',
  description: 'Mock description',
  creator: 'Jane Doe',
  owner: ['Web Team'],
  purpose: ['FY24/25 WE.1.1'],
  created_at: '2024-06-13T12:42:07.000Z',
  updated_at: '2024-06-13T12:42:07.000Z',
  utc_start_dt: '2024-06-01',
  utc_end_dt: '2024-06-30',
  task: 'https://phabricator.wikimedia.org/T368466',
  compliance_requirements: 'legal',
  contextual_attributes: '',
  contextual_attributes_map: {},
  sample_unit: 'pageview',
  sample_rate: '{"default":1}',
  environments: 'development',
  security_legal_review: 'pending',
  status: 0,
  stream_name: 'eventlogging_DesktopWebUIActionsTracking',
  schema_title: 'analytics/legacy/desktopwebuiactionstracking',
  schema_type: 'custom',
  email_address: 'contact@wikimedia.org',
  type: 'A/B test',
  variants:[{'name':'homepage_module','type':'boolean','values':[true, false]}],
  acknowledgement: true
};

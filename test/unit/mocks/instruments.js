'use strict';

exports.mockApiInstrumentsArray = 
[{
  "id": 1,
  "name": "test Web Scroll UI",
  "slug": "test-web-scroll-ui",
  "description": "abc description",
  "creator": "Jane Doe",
  "owner": ["Web Team"],
  "purpose": ["FY24/25 WE.1.1"],
  "created_at": "2024-09-17T11:45:40.000Z",
  "updated_at": "2024-09-17T11:45:40.000Z",
  "utc_start_dt": "2024-11-1T23:03:56.000Z",
  "utc_end_dt": "2024-11-30T23:05:45.000Z",
  "task": "https://phabricator.wikimedia.org/T369544",
  "compliance_requirements": "legal",
  "sample_unit": "pageview",
  "sample_rate": {
    "default": 1
  },
  "environments": "development",
  "security_legal_review": "pending",
  "status": 0,
  "stream_name": "mediawiki.web_ui_scroll_migrated",
  "schema_title": "analytics/product_metrics/web/base",
  "schema_type": "web",
  "email_address": "abstract@wikimedia.org",
  "contextual_attributes": [
    "page_id"
  ],
  "progress": 1
},
{
  "id": 2,
  "name": "test Desktop UI Interactions",
  "slug": "test-desktop-ui-interactions",
  "description": "efd description",
  "creator": "Jill Hill",
  "owner": ["Growth Team"],
  "purpose": ["FY24/25 WE.1.2"],
  "created_at": "2024-09-17T11:45:40.000Z",
  "updated_at": "2024-09-17T11:45:40.000Z",
  "utc_start_dt": "2024-05-01T00:00:00.000Z",
  "utc_end_dt": "2024-05-15T00:00:00.000Z",
  "task": "https://phabricator.wikimedia.org/T369544",
  "compliance_requirements": "gdpr",
  "sample_unit": "session",
  "sample_rate": {
    "default": 0.5,
    "0.1": [
      "frwiki"
    ],
    "0.01": [
      "enwiki"
    ]
  },
  "environments": "staging",
  "security_legal_review": "reviewed",
  "status": 0,
  "stream_name": "eventlogging_DesktopWebUIActionsTracking",
  "schema_title": "analytics/legacy/desktopwebuiactionstracking",
  "schema_type": "custom",
  "email_address": "web@wikimedia.org",
  "contextual_attributes": [
    "page_id"
  ],
  "progress": 1
},
{
  "id": 3,
  "name": "test Android Article Find In Page",
  "slug": "test-android-article-find-in-page",
  "description": "xyz description",
  "creator": "Julie Doe",
  "owner": ["Android Team"],
  "purpose": ["FY24/25 WE.1.3"],
  "created_at": "2024-09-17T11:45:40.000Z",
  "updated_at": "2024-09-17T11:45:40.000Z",
  "utc_start_dt": "2024-12-10T23:03:56.000Z",
  "utc_end_dt": "2024-12-30T23:05:45.000Z",
  "task": "https://phabricator.wikimedia.org/T369544",
  "compliance_requirements": "legal,gdpr",
  "sample_unit": "pageview",
  "sample_rate": {
    "default": 1
  },
  "environments": "production",
  "security_legal_review": "pending",
  "status": 0,
  "stream_name": "android.find_in_page_interaction",
  "schema_title": "analytics/mobile_apps/android_find_in_page_interaction",
  "schema_type": "custom",
  "email_address": "android@wikimedia.org",
  "contextual_attributes": [
    "page_id"
  ],
  "progress": 1
},
{
  "id": 4,
  "name": "test Android Article Link Preview Interaction",
  "slug": "test-android-article-link-preview-interaction",
  "description": "www description",
  "creator": "Jim Doe",
  "owner": ["Android Team"],
  "purpose": ["FY24/25 SDS.2.1"],
  "created_at": "2024-09-17T11:45:40.000Z",
  "updated_at": "2024-09-17T11:45:40.000Z",
  "utc_start_dt": "2024-05-01T23:03:56.000Z",
  "utc_end_dt": "2024-05-31T23:05:45.000Z",
  "task": "https://phabricator.wikimedia.org/T369544",
  "compliance_requirements": "legal,gdpr",
  "sample_unit": "pageview",
  "sample_rate": {
    "default": 0.5
  },
  "environments": "production",
  "security_legal_review": "reviewed",
  "status": 0,
  "stream_name": "android.product_metrics.article_link_preview_interaction",
  "schema_title": "analytics/product_metrics/app/base",
  "schema_type": "app",
  "email_address": "android@wikimedia.org",
  "contextual_attributes": [
    "page_namespace",
    "page_revision_id",
    "page_wikidata_qid",
    "page_is_redirect",
    "page_user_groups_allowed_to_edit",
    "mediawiki_skin"
  ],
  "progress": 1
},
{
  "id": 5,
  "name": "test Android Article TOC",
  "slug": "test-android-article-toc",
  "description": "eee description",
  "creator": "John Doe",
  "owner": ["Android Team"],
  "purpose": ["FY24/25 SDS.2.2"],
  "created_at": "2024-09-17T11:45:40.000Z",
  "updated_at": "2024-09-17T11:45:40.000Z",
  "utc_start_dt": "2024-09-10T23:03:56.000Z",
  "utc_end_dt": "2024-09-30T23:05:45.000Z",
  "task": "https://phabricator.wikimedia.org/T369544",
  "compliance_requirements": "legal",
  "sample_unit": "pageview",
  "sample_rate": {
    "default": 1
  },
  "environments": "development",
  "security_legal_review": "reviewd",
  "status": 0,
  "stream_name": "android.product_metrics.article_toc_interaction",
  "schema_title": "analytics/mobile_apps/product_metrics/android_article_toc_interaction",
  "schema_type": "custom",
  "email_address": "android@wikimedia.org",
  "contextual_attributes": [],
  "progress": 1
}
];

exports.mockInstrumentsArray = [
    { id: 1, name: 'test Web Scroll UI', slug: 'test-web-scroll-ui', description: 'abc description', creator: 'Jane Doe', owner: ['Web Team'], purpose: ['WE.1.1'], created_at: '2024-08-01T10:20:22.000Z', updated_at: '2024-08-01T10:20:22.000Z', utc_start_dt: '2024-03-01T23:05:45.000Z', utc_end_dt: '2024-03-30T23:05:45.000Z', task: 'https://phabricator.wikimedia.org/T369544', compliance_requirements: 'legal', sample_unit: 'pageview', sample_rate: { default: 1 }, environments: 'development', security_legal_review: 'pending', status: 0, stream_name: 'mediawiki.web_ui_scroll_migrated', schema_title: 'analytics/product_metrics/web/base', schema_type: 'web', email_address: 'abstract@wikimedia.org', type: 'instrument', contextual_attributes: ['page_id', 'session_id'], progress: 1 },
    { id: 2, name: 'test Desktop UI Interactions', slug: 'test-desktop-ui-interactions', description: 'efd description', creator: 'Jill Hill', owner: ['Growth Team'], purpose: ['WE.1.2'], created_at: '2024-08-01T10:20:22.000Z', updated_at: '2024-08-01T10:20:22.000Z', utc_start_dt: '2024-06-01T23:05:45.000Z', utc_end_dt: '2024-06-30T23:05:45.000Z', task: 'https://phabricator.wikimedia.org/T369544', compliance_requirements: 'gdpr', sample_unit: 'session', sample_rate: { default: 0.5, 0.1: ['frwiki'], 0.01: ['enwiki'] }, environments: 'staging', security_legal_review: 'reviewed', status: 0, stream_name: 'eventlogging_DesktopWebUIActionsTracking', schema_title: 'analytics/legacy/desktopwebuiactionstracking', schema_type: 'custom', email_address: 'web@wikimedia.org', type: 'instrument', contextual_attributes: ['page_id', 'session_id'], progress: 1 },
    { id: 3, name: 'test Android Article Find In Page', slug: 'test-android-article-find-in-page', description: 'xyz description', creator: 'Julie Doe', owner: ['Android Team'], purpose: ['WE.1.3'], created_at: '2024-08-01T10:20:22.000Z', updated_at: '2024-08-01T10:20:22.000Z', utc_start_dt: '2024-09-01T23:05:45.000Z', utc_end_dt: '2024-09-30T23:05:45.000Z', task: 'https://phabricator.wikimedia.org/T369544', compliance_requirements: 'legal,gdpr', sample_unit: 'pageview', sample_rate: { default: 1 }, environments: 'production', security_legal_review: 'pending', status: 0, stream_name: 'android.find_in_page_interaction', schema_title: 'analytics/mobile_apps/android_find_in_page_interaction', schema_type: 'custom', email_address: 'android@wikimedia.org', type: 'instrument', contextual_attributes: ['page_id'], progress: 1 },
    { id: 4, name: 'test Android Article Link Preview Interaction', slug: 'test-android-article-link-preview-interaction', description: 'www description', creator: 'Jim Doe', owner: ['Android Team'], purpose: ['SDS.2.1'], created_at: '2024-08-01T10:20:22.000Z', updated_at: '2024-08-01T10:20:22.000Z', utc_start_dt: '2024-05-01T23:05:45.000Z', utc_end_dt: '2024-05-30T23:05:45.000Z', dutask: 'https://phabricator.wikimedia.org/T369544', compliance_requirements: 'legal,gdpr', sample_unit: 'pageview', sample_rate: { default: 0.5 }, environments: 'production', security_legal_review: 'reviewed', status: 0, stream_name: 'android.product_metrics.article_link_preview_interaction', schema_title: 'analytics/product_metrics/app/base', schema_type: 'app', email_address: 'android@wikimedia.org', type: 'instrument', contextual_attributes: ['page_namespace', 'page_revision_id', 'page_wikidata_qid', 'page_is_redirect', 'page_user_groups_allowed_to_edit', 'mediawiki_skin'], progress: 1 },
    { id: 5, name: 'test Android Article TOC', slug: 'test-android-article-toc', description: 'eee description', creator: 'John Doe', owner: ['Android Team'], purpose: ['SDS.2.2'], created_at: '2024-08-01T10:20:22.000Z', updated_at: '2024-08-01T10:20:22.000Z', utc_start_dt: '2024-08-01T23:05:45.000Z', utc_end_dt: '2024-08-31T23:05:45.000Z', task: 'https://phabricator.wikimedia.org/T369544', compliance_requirements: 'legal', sample_unit: 'pageview', sample_rate: { default: 1 }, environments: 'development', security_legal_review: 'reviewd', status: 0, stream_name: 'android.product_metrics.article_toc_interaction', schema_title: 'analytics/mobile_apps/product_metrics/android_article_toc_interaction', schema_type: 'custom', email_address: 'android@wikimedia.org', type: 'instrument', contextual_attributes: ['mediawiki_version', 'page_id'], progress: 1 }
];

exports.mockInstrument = {
    id: 12,
    name: 'Mock Instrument',
    slug: 'mock-instrument',
    description: 'Mock description',
    creator: 'Jane Doe',
    owner: ['Web Team'],
    purpose: ['FY24/25 WE.1.1'],
    created_at: '2024-06-13T12:42:07.000Z',
    updated_at: '2024-06-13T12:42:07.000Z',
    utc_start_dt: '2024-09-01',
    utc_end_dt: '2024-09-30',
    task: 'https://phabricator.wikimedia.org/T368466',
    compliance_requirements: 'legal',
    contextual_attributes: 'page_id',
    contextual_attributes_map: { 1: "page_id"},
    sample_unit: 'pageview',
    sample_rate: '{"default":1}',
    environments: 'development',
    security_legal_review: 'pending',
    status: 0,
    stream_name: 'eventlogging_DesktopWebUIActionsTracking',
    schema_title: 'analytics/legacy/desktopwebuiactionstracking',
    schema_type: 'custom',
    email_address: 'contact@wikimedia.org',
    type: 'instrument',
    acknowledgement: true
};

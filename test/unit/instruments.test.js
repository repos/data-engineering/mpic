'use strict';

const httpMocks = require('node-mocks-http');
const { describe, it, expect, afterEach } = require('@jest/globals');
const _ = require('lodash');
const testUtil = require('../testUtil');

jest.mock('../../service/instrumentService');

const instrumentController = require('../../controller/instrumentController');
const instrumentService = require('../../service/instrumentService');
const { mockApiInstrumentsArray, mockInstrumentsArray, mockInstrument } = require('./mocks/instruments');
const { mockExperimentArray }  = require('./mocks/experiments');
const mockFindAllInstruments = jest.spyOn(instrumentService, 'findAllInstruments');
const mockAddInstrument = jest.spyOn(instrumentService, 'addInstrument');
const mockFindExistingInstrument = jest.spyOn(instrumentService, 'findExistingInstrument');
const mockFindInstrument = jest.spyOn(instrumentService, 'findInstrument');
const mockUpdateInstrument = jest.spyOn(instrumentService, 'updateInstrument');
const mockRemoveInstrument = jest.spyOn(instrumentService, 'removeInstrument');
const mockActivateInstrument = jest.spyOn(instrumentService, 'activateInstrument');
const mockDeactivateInstrument = jest.spyOn(instrumentService, 'deactivateInstrument');

afterEach(() => {
    jest.clearAllMocks();
});

beforeAll(() => {

});

describe('Instruments controller', () => {
    it('GET /api/v1/instruments should get instrument list', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.path = '/api/v1/instruments';

        const mockInstrumentList = jest.fn(async () => {
            return mockApiInstrumentsArray;
        });

        mockFindAllInstruments.mockImplementation(mockInstrumentList);

        await instrumentController.getInstruments(request, response);
        expect(mockFindAllInstruments).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getData().length).toEqual(5);
    });

    it('GET /instruments should get instrument list including utc_start_dt and utc_end_dt properties', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.path = '/instruments';

        const mockResponse = jest.fn(async () => {
            return mockInstrumentsArray;
        });

        mockFindAllInstruments.mockImplementation(mockResponse);

        await instrumentController.getInstruments(request, response);
        expect(mockFindAllInstruments).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getData().length).toEqual(5);
        expect(response._getData()[0].utc_start_dt).toBeTruthy();
        expect(response._getData()[0].utc_end_dt).toBeTruthy();
    });

    it('GET /instruments should include type properties with "A/B test" and "instrument"', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.path = '/instruments';

        const mockResponse = jest.fn(async () => {
            return [...mockInstrumentsArray, ...mockExperimentArray];
        });

        mockFindAllInstruments.mockImplementation(mockResponse);

        await instrumentController.getInstruments(request, response);

        expect(mockFindAllInstruments).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200); // Make sure it hits this point
        expect(response._isEndCalled()).toBeTruthy();

        const instruments = response._getData();

        // Check specific type values
        const abTestInstrument = instruments.find(inst => inst.type === 'A/B test');
        const baselineInstrument = instruments.find(inst => inst.type === 'instrument');

        expect(abTestInstrument).toBeTruthy();
        expect(baselineInstrument).toBeTruthy();

        expect(abTestInstrument.type).toEqual('A/B test');
        expect(baselineInstrument.type).toEqual('instrument');
    });



    it('GET /instrument/slug should get a specific instrument', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.slug = 'mock-instrument';

        // Validate request
        const validationResult = await testUtil.validateSlugRequestParam(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockResponse = jest.fn(async () => {
            return mockInstrument;
        });

        mockFindInstrument.mockImplementation(mockResponse);

        await instrumentController.getInstrument(request, response);
        expect(mockFindInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().name).toEqual('Mock Instrument');
        expect(response._getJSONData().creator).toEqual('Jane Doe');
        expect(response._getJSONData().owner[0]).toEqual('Web Team');
        expect(response._getJSONData().contextual_attributes_map).toEqual(mockInstrument.contextual_attributes_map);
    });

    it('GET /instrument/slug should fail when the instrument doesn\'t exist', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.slug = 'i-dont-exist';

        // Validate request
        const validationResult = await testUtil.validateSlugRequestParam(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockResponse = jest.fn(async () => {
            return null;
        });

        mockFindInstrument.mockImplementation(mockResponse);

        await instrumentController.getInstrument(request, response);
        expect(mockFindInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(404);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument i-dont-exist not found');
    });

    it('GET /instrument/slug should fail when slug is not alphanumeric', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.slug = 'wrongslug**';

        // Validate request
        const validationResult = await testUtil.validateSlugRequestParam(request);
        expect(validationResult.errors.length).toEqual(1);

        await instrumentController.getInstrument(request, response);
        expect(mockFindInstrument).toHaveBeenCalledTimes(0);
        expect(response.statusCode).toEqual(400);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('There are some errors while validating the request (invalid slug)');
    });

    it('GET /api/v1/instruments should not include type in the response for all instruments', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.path = '/api/v1/instruments';

        const mockInstrumentList = jest.fn(async () => {
            return mockApiInstrumentsArray;
        });

        mockFindAllInstruments.mockImplementation(mockInstrumentList);

        await instrumentController.getInstruments(request, response);
        expect(mockFindAllInstruments).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200);
        expect(response._isEndCalled()).toBeTruthy();

        const instruments = response._getData();
        expect(instruments.length).toBeGreaterThan(0);

        instruments.forEach(instrument => {
            expect(instrument.type).toBeUndefined();
        });
    });

    it('POST /instruments should register a new instrument', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.user = {
            id: 99
        };
        request.body = Object.assign(_.pick(mockInstrument, testUtil.POST_INSTRUMENT_COLUMNS));
        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockAddInstrumentResponse = jest.fn(async () => {
            return true;
        });
        const mockFindExistingInstrumentResponse = jest.fn(async () => {
            return [];
        });

        mockAddInstrument.mockImplementation(mockAddInstrumentResponse);
        mockFindExistingInstrument.mockImplementation(mockFindExistingInstrumentResponse);

        await instrumentController.postInstrument(request, response);
        expect(mockAddInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(201);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument registered successfully');
    });

    it('POST /instruments should register a new instrument with default contextual_attributes', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.user = {
            id: 99
        };

        // Omitting contextual_attributes to check if it's set by default
        const instrumentWithoutContextualAttributes = _.omit(mockInstrument, ['contextual_attributes']);
        request.body = Object.assign(_.pick(instrumentWithoutContextualAttributes, testUtil.POST_INSTRUMENT_COLUMNS));

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockAddInstrumentResponse = jest.fn(async () => {
            return true;
        });
        const mockFindExistingInstrumentResponse = jest.fn(async () => {
            return [];
        });

        mockAddInstrument.mockImplementation(mockAddInstrumentResponse);
        mockFindExistingInstrument.mockImplementation(mockFindExistingInstrumentResponse);

        await instrumentController.postInstrument(request, response);
        expect(mockAddInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(201);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument registered successfully');

    });



    it('POST /instruments should fail (409) when an existing instrument exists with the same name', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.user = {
            id: 99
        };
        request.body = Object.assign(_.pick(mockInstrument, testUtil.POST_INSTRUMENT_COLUMNS));

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockFindInstrumentResponse = jest.fn(async () => {
            return mockInstrument;
        });

        mockFindExistingInstrument.mockImplementation(mockFindInstrumentResponse);

        await instrumentController.postInstrument(request, response);
        expect(mockFindExistingInstrument).toHaveBeenCalledTimes(1);
        expect(mockAddInstrument).toHaveBeenCalledTimes(0);
        expect(response.statusCode).toEqual(409);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('There is already an instrument with the same name/slug');
    });

    it('POST /instruments should fail (400) when request doesn\'t validate', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.user = {
            id: 99
        };
        request.body = Object.assign(_.pick(mockInstrument, testUtil.POST_INSTRUMENT_COLUMNS), {
            name: '',
            sample_unit: ''
        });

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toBeGreaterThan(0);

        // No more mocks needed. Validation should fail
        await instrumentController.postInstrument(request, response);
        expect(mockAddInstrument).toHaveBeenCalledTimes(0);
        expect(response.statusCode).toEqual(400);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('There are some errors while validating the form');
    });

    it('POST /instruments should fail with a 500 error when registration fails for some reason', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.user = {
            id: 99
        };
        request.body = Object.assign(_.pick(mockInstrument, testUtil.POST_INSTRUMENT_COLUMNS), {
            name: 'a new instrument'
        });

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockAddInstrumentResponse = jest.fn(async () => {
            throw new Error('something failed while registering the instrument in the database');
        });
        const mockFindExistingInstrumentResponse = jest.fn(async () => {
            return [];
        });

        mockAddInstrument.mockImplementation(mockAddInstrumentResponse);
        mockFindExistingInstrument.mockImplementation(mockFindExistingInstrumentResponse);

        // No more mocks needed. Validation should fail
        await instrumentController.postInstrument(request, response);
        expect(mockAddInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(500);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Error while registering the instrument');
    });

    it('PUT /instruments/:slug should modify an existing instrument', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.path = '/instrument/mock-instrument';
        request.user = {
            id: 99
        };
        request.body = Object.assign(_.pick(mockInstrument, testUtil.PUT_INSTRUMENT_COLUMNS));

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockUpdateInstrumentResponse = jest.fn(async () => {
            return true;
        });

        mockUpdateInstrument.mockImplementation(mockUpdateInstrumentResponse);

        await instrumentController.putInstrument(request, response);
        expect(mockUpdateInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument updated successfully');
    });

    it('PUT /instruments/:slug should fail if the request doesn\'t validate', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.path = '/instrument/mock-instrument';
        request.user = {
            id: 99
        };
        request.body = Object.assign(_.pick(mockInstrument, testUtil.PUT_INSTRUMENT_COLUMNS), {
            name: '',
            sample_rate: ''
        });

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toEqual(2);

        const mockUpdateInstrumentResponse = jest.fn(async () => {
            return true;
        });

        mockUpdateInstrument.mockImplementation(mockUpdateInstrumentResponse);

        await instrumentController.putInstrument(request, response);
        expect(mockUpdateInstrument).toHaveBeenCalledTimes(0);
        expect(response.statusCode).toEqual(400);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('There are some errors while validating the form');
    });

    it('PUT /instruments/:slug should fail if the instrument doesn\'t exist', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.path = '/instrument/idontexist';
        request.user = {
            id: 99
        };
        request.body = Object.assign(_.pick(mockInstrument, testUtil.PUT_INSTRUMENT_COLUMNS));

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockUpdateInstrumentResponse = jest.fn(async () => {
            return false;
        });

        mockUpdateInstrument.mockImplementation(mockUpdateInstrumentResponse);

        await instrumentController.putInstrument(request, response);
        expect(mockUpdateInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(404);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument not found');
    });

    it('PUT /instruments/:slug should fail with a 500 error when contextual attributes setting fails', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.path = '/instrument/idontexist';
        request.user = {
            id: 99
        };
        request.body = Object.assign(_.pick(mockInstrument, testUtil.PUT_INSTRUMENT_COLUMNS));

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toEqual(0);

        const mockUpdateInstrumentResponse = jest.fn(async () => {
            throw new Error('contextual attributes setting has failed while updating the instrument');
        });

        mockUpdateInstrument.mockImplementation(mockUpdateInstrumentResponse);

        await instrumentController.putInstrument(request, response);
        expect(mockUpdateInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(500);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Error: contextual attributes setting has failed while updating the instrument');
    });

    it('DELETE /instruments/:id should remove an instrument with a 200 response', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.id = 11;

        const mockResponse = jest.fn(async () => {
            return 1;
        });

        mockRemoveInstrument.mockImplementation(mockResponse);

        await instrumentController.deleteInstrument(request, response);
        expect(mockRemoveInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument deleted successfully');
    });

    it('DELETE /instruments/:id should fail with 404 when the instrument doesn\'t exist', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.id = 6;

        const mockResponse = jest.fn(async () => {
            return null;
        });

        mockRemoveInstrument.mockImplementation(mockResponse);

        await instrumentController.deleteInstrument(request, response);
        expect(mockRemoveInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(404);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument not found');
    });

    it('PATCH /instruments/:id should activate an instrument', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.id = 6;
        request.body.status = true;

        const mockResponse = jest.fn(async () => {
            return 1;
        });

        mockActivateInstrument.mockImplementation(mockResponse);

        await instrumentController.patchInstrumentStatus(request, response);
        expect(mockActivateInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument activated successfully');
    });

    it('PATCH /instruments/:id should deactivate an instrument', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.id = 6;
        request.body.status = false;

        const mockResponse = jest.fn(async () => {
            return 1;
        });

        mockDeactivateInstrument.mockImplementation(mockResponse);

        await instrumentController.patchInstrumentStatus(request, response);
        expect(mockDeactivateInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(200);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Instrument deactivated successfully');
    });

    it('PATCH /instruments/:id should fail with a 500 status code when there is an error trying to activate an instrument', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.id = 6;
        request.body.status = true;

        const mockResponse = jest.fn(async () => {
            throw new Error('Error while enabling the instrument');
        });

        mockActivateInstrument.mockImplementation(mockResponse);

        await instrumentController.patchInstrumentStatus(request, response);
        expect(mockActivateInstrument).toHaveBeenCalledTimes(1);
        expect(response.statusCode).toEqual(500);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('Error: Error while enabling the instrument');
    });

    it('GET /instrument/slug should fail when slug is not alphanumeric', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.params.slug = 'wrongslug**';

        // Validate request
        const validationResult = await testUtil.validateSlugRequestParam(request);
        expect(validationResult.errors.length).toEqual(1); // Ensure that there's an error for invalid slug

        await instrumentController.getInstrument(request, response);
        expect(mockFindInstrument).toHaveBeenCalledTimes(0); // Shouldn't call the service if slug is invalid
        expect(response.statusCode).toEqual(400); // Bad request status
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('There are some errors while validating the request (invalid slug)');
    });

    it('POST /instruments should fail (400) when name contains invalid characters', async () => {
        const response = httpMocks.createResponse();
        const request = testUtil.prepareRequest();
        request.user = { id: 99 };

        // Invalid name with special characters
        request.body = Object.assign(_.pick(mockInstrument, testUtil.POST_INSTRUMENT_COLUMNS), {
            name: 'Invalid#Name!',
        });

        // Validate request
        const validationResult = await testUtil.validateBodyRequest(request);
        expect(validationResult.errors.length).toBeGreaterThan(0);

        // No more mocks needed. Validation should fail
        await instrumentController.postInstrument(request, response);
        expect(mockAddInstrument).toHaveBeenCalledTimes(0);
        expect(response.statusCode).toEqual(400);
        expect(response._isEndCalled()).toBeTruthy();
        expect(response._getJSONData().title).toEqual('There are some errors while validating the form');
    });


});

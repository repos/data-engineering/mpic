'use strict';

const httpMocks = require('node-mocks-http');
const { validationResult } = require('express-validator');

const { instrumentBodyValidator, instrumentSlugParamValidator } = require('../routes/validators');
const { logger } = require('../config/configuration');

// Prepare the request to be use within the unit tests
function prepareRequest() {
    const request = httpMocks.createRequest();
    request.app = {};
    request.app.logger = logger;
    request.app.conf = {};

    return request;
}

// Validate body request using express-validator
async function validateBodyRequest(request) {
    for (const validation of instrumentBodyValidator) {
        await validation.run(request);
    }
    return validationResult(request);
}

// Validate request param slug using express-validator
async function validateSlugRequestParam(request) {
    for (const validation of instrumentSlugParamValidator) {
        await validation.run(request);
    }
    return validationResult(request);
}

const POST_INSTRUMENT_COLUMNS = [
	'name',
	'slug',
	'description',
	'owner',
	'purpose',
	'utc_start_dt',
	'utc_end_dt',
	'task',
	'compliance_requirements',
	'contextual_attributes',
    'contextual_attributes_map',
	'sample_unit',
	'sample_rate',
	'environments',
	'security_legal_review',
	'status',
	'stream_name',
	'schema_title',
	'schema_type',
	'email_address',
	'type',
	'acknowledgement'
];

const PUT_INSTRUMENT_COLUMNS = [
	'id',
	'name',
	'slug',
	'description',
	'creator',
	'owner',
	'purpose',
	'updated_at',
	'utc_start_dt',
	'utc_end_dt',
	'task',
	'compliance_requirements',
    'contextual_attributes_map',
	'sample_unit',
	'sample_rate',
	'environments',
	'security_legal_review',
	'status',
	'stream_name',
	'schema_title',
	'schema_type',
	'email_address',
	'type',
	'acknowledgement'
];
const POST_EXPERIMENT_COLUMNS = [
	'name',
	'slug',
	'description',
	'owner',
	'purpose',
	'utc_start_dt',
	'utc_end_dt',
	'task',
	'compliance_requirements',
	'contextual_attributes',
    'contextual_attributes_map',
	'sample_unit',
	'sample_rate',
	'environments',
	'security_legal_review',
	'status',
	'stream_name',
	'schema_title',
	'schema_type',
	'email_address',
	'type',
	'variants',
	'acknowledgement'
];

const PUT_EXPERIMENT_COLUMNS = [
	'id',
	'name',
	'slug',
	'description',
	'creator',
	'owner',
	'purpose',
	'updated_at',
	'utc_start_dt',
	'utc_end_dt',
	'task',
	'compliance_requirements',
    'contextual_attributes_map',
	'sample_unit',
	'sample_rate',
	'environments',
	'security_legal_review',
	'status',
	'stream_name',
	'schema_title',
	'schema_type',
	'email_address',
	'type',
	'variants',
	'acknowledgement'
];

module.exports = {
	prepareRequest,
    validateBodyRequest,
    validateSlugRequestParam,
    POST_INSTRUMENT_COLUMNS,
	PUT_INSTRUMENT_COLUMNS,
	POST_EXPERIMENT_COLUMNS,
	PUT_EXPERIMENT_COLUMNS
};

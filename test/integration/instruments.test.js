'use strict';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const dotenv = require('dotenv');
const _ = require('lodash');

const { mockInstrument } = require('../unit/mocks/instruments');
const { UTCDate } = require( "@date-fns/utc");
const { format } = require("date-fns");
const testUtil = require('../testUtil');

dotenv.config({ path: '.env.test.local', override: true });
const app = require('../../app').app;

chai.use(chaiHttp);
chai.should();

describe('Instruments', () => {
    describe('GET /instruments', () => {
        it('should get all instruments', (done) => {
            chai.request(app)
                .get('/instruments')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    expect(res.body[0].name).to.equal('test Web Scroll UI');
                    expect(res.body[0]).to.have.property('slug');
                    expect(res.body[0]).to.have.property('description');
                    expect(res.body[0]).to.have.property('type');
                    done();
                });
        });
    });

    describe('GET /api/v1/instruments', () => {
        it('should get all instruments', (done) => {
            chai.request(app)
                .get('/api/v1/instruments')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    expect(res.body[0].name).to.equal('test Web Scroll UI');
                    expect(res.body[0]).to.not.have.property('variants');
                    expect(res.body[0]).to.not.have.property('type');
                    done();
                });
        });
    });

    describe('GET /instrument/test-web-scroll-ui', () => {
        it('should get an instrument called test-web-scroll-ui', (done) => {
            chai.request(app)
                .get('/instrument/test-web-scroll-ui')
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.body.name).to.equal('test Web Scroll UI');
                    expect(res.body.slug).to.equal('test-web-scroll-ui');
                    done();
                });
        });
    });

    describe('GET a nonexistent instrument', () => {
        it('should get a 404 Not Found error when trying to get a non-existent instrument', (done) => {
            chai.request(app)
                .get('/instrument/nonexistent')
                .end((err, res) => {
                    res.should.have.status(404);
                    expect(res.body.title).to.equal('Instrument nonexistent not found');
                    done();
                });
        });
    });

    describe('POST /instruments', () => {
        it('a new instrument should be registered with a 201 response', (done) => {
            const instrumentData = Object.assign(_.pick(mockInstrument,
                testUtil.POST_INSTRUMENT_COLUMNS));

            chai.request(app)
                .post('/instruments')
                .send(instrumentData)
                .end((err, res) => {
                    res.should.have.status(201);
                    expect(res.body.title).to.equal('Instrument registered successfully');
                    done();
                });
        });

        it('the registered instrument above is correct', (done) => {
            chai.request(app)
                .get('/instrument/' + mockInstrument.slug)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.body.name).to.equal(mockInstrument.name);
                    expect(res.body.slug).to.equal(mockInstrument.slug);
                    expect(res.body.description).to.equal(mockInstrument.description);
                    expect(res.body.creator).to.equal('testuser');
                    expect(res.body.owner).to.deep.equal(mockInstrument.owner);
                    expect(res.body.purpose).to.deep.equal(mockInstrument.purpose);
                    expect(format(res.body.created_at, "yyyy-MM-dd")).to.equal(format(new UTCDate(), "yyyy-MM-dd"));
                    expect(format(res.body.updated_at, "yyyy-MM-dd")).to.equal(format(new UTCDate(), "yyyy-MM-dd"));
                    expect(format(res.body.utc_start_dt, "yyyy-MM-dd")).to.equal(format(new UTCDate(mockInstrument.utc_start_dt), "yyyy-MM-dd"));
                    expect(format(res.body.utc_end_dt, "yyyy-MM-dd")).to.equal(format(new UTCDate(mockInstrument.utc_end_dt), "yyyy-MM-dd"));
                    expect(res.body.task).to.equal(mockInstrument.task);
                    expect(res.body.compliance_requirements).to.equal(mockInstrument.compliance_requirements);
                    expect(res.body.contextual_attributes).to.equal(mockInstrument.contextual_attributes);
                    expect(res.body.sample_unit).to.equal(mockInstrument.sample_unit);
                    expect(JSON.stringify(res.body.sample_rate)).to.equal(mockInstrument.sample_rate);
                    expect(res.body.environments).to.equal(mockInstrument.environments);
                    expect(res.body.security_legal_review).to.equal(mockInstrument.security_legal_review);
                    expect(res.body.status).to.equal(mockInstrument.status);
                    expect(res.body.stream_name).to.equal(mockInstrument.stream_name);
                    expect(res.body.schema_title).to.equal(mockInstrument.schema_title);
                    expect(res.body.email_address).to.equal(mockInstrument.email_address);
                    expect(res.body.type).to.equal(mockInstrument.type);
                    done();
                });
        });

        it('instrument registration should fail with a 400 bad request error if validation fails', (done) => {
            const instrumentData = Object.assign(_.pick(mockInstrument,
                testUtil.POST_INSTRUMENT_COLUMNS), {
                description: '',
                sample_rate: '',
                task: ''
            });

            chai.request(app)
                .post('/instruments')
                .send(instrumentData)
                .end((err, res) => {
                    res.should.have.status(400);
                    expect(res.body.title).to.equal('There are some errors while validating the form');
                    done();
                });
        });

        it('instrument registration should fail with a 409 status code when if there is already an existing instrument with the same name', (done) => {
            const instrumentData = Object.assign(_.pick(mockInstrument,
                testUtil.POST_INSTRUMENT_COLUMNS));

            chai.request(app)
                .post('/instruments')
                .send(instrumentData)
                .end((err, res) => {
                    res.should.have.status(409);
                    expect(res.body.title).to.equal('There is already an instrument with the same name/slug');
                    done();
                });
        });
    });

    describe('PUT /instrument/:slug', () => {
        it('an existing instrument should be updated with a 200 response', (done) => {
            const instrumentData = Object.assign(_.pick(mockInstrument,
                testUtil.PUT_INSTRUMENT_COLUMNS), {
                description: 'Modified description'
            });

            chai.request(app)
                .put('/instrument/' + mockInstrument.slug)
                .send(instrumentData)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.body.title).to.equal('Instrument updated successfully');
                    done();
                });
        });

        it('instrument updating should fail with a 400 bad request error if validation fails', (done) => {
            const instrumentData = Object.assign(_.pick(mockInstrument,
                testUtil.PUT_INSTRUMENT_COLUMNS), {
                description: '',
                status: ''
            });

            chai.request(app)
                .put('/instrument/' + mockInstrument.slug)
                .send(instrumentData)
                .end((err, res) => {
                    res.should.have.status(400);
                    expect(res.body.title).to.equal('There are some errors while validating the form');
                    done();
                });
        });
    });

    describe('PATCH /instrument/:id', () => {
        it('An instrument should be activated with a 200 response', (done) => {
            const instrumentData = {
                status: true
            };

            chai.request(app)
                .patch('/instrument/1')
                .send(instrumentData)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.body.title).to.equal('Instrument activated successfully');
                    done();
                });
        });

        it('An instrument should be disabled with a 200 response', (done) => {
            const instrumentData = {
                status: false
            };

            chai.request(app)
                .patch('/instrument/1')
                .send(instrumentData)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.body.title).to.equal('Instrument deactivated successfully');
                    done();
                });
        });
    });

    describe('DELETE /instrument/:id', () => {
        it('An instrument should be deleted with a 200 response', (done) => {
            chai.request(app)
                .delete('/instrument/' + mockInstrument.id)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.body.title).to.equal('Instrument deleted successfully');
                    done();
                });
        });
    });
});

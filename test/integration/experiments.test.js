'use strict';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const dotenv = require('dotenv');
const _ = require('lodash');
const { mockExperiment,mockApiExperimentArray } = require('../unit/mocks/experiments');
const testUtil = require('../testUtil');

dotenv.config({ path: '.env.test.local', override: true });
const app = require('../../app').app;

chai.use(chaiHttp);
chai.should();

describe('Experiments', () => {
    
    describe('POST /experiments', () => {
        it('should register a new experiment with a 201 response', (done) => {
            const experimentData = Object.assign(_.pick(mockExperiment, testUtil.POST_EXPERIMENT_COLUMNS));

            chai.request(app)
                .post('/experiments')
                .send(experimentData)
                .end((err, res) => {
                    res.should.have.status(201);
                    expect(res.body.title).to.equal('Instrument registered successfully');
                    done();
                });
        });

        it('should fail with a 400 bad request error if validation fails', (done) => {
            const experimentData = Object.assign(_.pick(mockExperiment, testUtil.POST_EXPERIMENT_COLUMNS), {
                utc_start_dt: '',
                sample_rate: '',
                task: ''
            });

            chai.request(app)
                .post('/experiments')
                .send(experimentData)
                .end((err, res) => {
                    res.should.have.status(400);
                    expect(res.body.title).to.equal('There are some errors while validating the form');
                    done();
                });
        });

        it('should fail with a 409 status code if an experiment with the same name/slug exists', (done) => {
            const experimentData = Object.assign(_.pick(mockExperiment, testUtil.POST_EXPERIMENT_COLUMNS));

            chai.request(app)
                .post('/experiments')
                .send(experimentData)
                .end((err, res) => {
                    res.should.have.status(409);
                    expect(res.body.title).to.equal('There is already an instrument with the same name/slug');
                    done();
                });
        });
    });

    describe('PUT /experiment/:slug', () => {
        it('should update an existing experiment with a 200 response', (done) => {
            const experimentData = Object.assign(_.pick(mockExperiment, testUtil.PUT_EXPERIMENT_COLUMNS), {
                description: 'Updated description'
            });

            chai.request(app)
                .put('/experiment/' + mockExperiment.slug)
                .send(experimentData)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res.body.title).to.equal('Instrument updated successfully');
                    done();
                });
        });

        it('should fail with a 400 bad request error if validation fails', (done) => {
            const experimentData = Object.assign(_.pick(mockExperiment, testUtil.PUT_EXPERIMENT_COLUMNS), {
                description: '',
                status: ''
            });

            chai.request(app)
                .put('/experiment/' + mockExperiment.slug)
                .send(experimentData)
                .end((err, res) => {
                    res.should.have.status(400);
                    expect(res.body.title).to.equal('There are some errors while validating the form');
                    done();
                });
        });
    });

    describe('GET /api/v1/experiments', () => {
        it('should get all experiments', (done) => {
            chai.request(app)
                .get('/api/v1/experiments')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    expect(res.body[0].name).to.equal(mockApiExperimentArray[0].name);
                    done();
                });
        });
    });

    describe('GET /experiments', () => {
        it('should get all experiments (authenticated)', (done) => {
            chai.request(app)
                .get('/experiments')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    expect(res.body[0].name).to.equal(mockApiExperimentArray[0].name);
                    done();
                });
        });
    });
});

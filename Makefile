# Copyright 2024 Wikimedia Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

UNAME := $(shell uname)
ifeq ($(UNAME), Linux)
	COMPOSE := docker compose
else
	COMPOSE := docker-compose
endif

# Create the MPIC docker image for production-like environments
mpic-qa:
	docker build --target production -f .pipeline/blubber.yaml -t mpic .

# Start the MPIC docker test environment for production
# To avoid cache issues, you can run `docker builder prune` before creating this environment
startup-qa: mpic-qa
	rm -rf public
	$(COMPOSE) up -d

# Stop and remove the MPIC docker test environment for production
shutdown-qa:
	$(COMPOSE) down --remove-orphans -v
	docker rmi mpic

# Start the MPIC docker development environment (only the database)
startup-dev:
	$(COMPOSE) -f docker-compose.dev.yaml up -d

# Stop and remove the MPIC docker development environment
shutdown-dev:
	$(COMPOSE) -f docker-compose.dev.yaml down --remove-orphans -v

# Run the development environment (only database) and MPIC in development mode
run-dev: startup-dev
	npm start

# Run the development environment (only database) and MPIC in production mode
run-prod: startup-dev
	npm run start-prod

# Run all tests (unit and integration ones) after rebuilding the test environment
all-test: shutdown-dev startup-dev
	sleep 4
	npm test

.PHONY: mpic startup-qa shutdown-qa startup-dev shutdown-dev run-dev run-prod run-integration-test


import axios from 'axios';

export default function useUtility() {

  function getSlugFormatted(instrument_name) {
    return instrument_name.replace(/\s+/g, '-').toLowerCase();
  }

  function splitStringAttributes(input) {
    return (input ?? '').split(',').filter(x => x.length > 0)
  }

  // Get the stream title given the sream name making a request to the Action API (See https://meta.wikimedia.org/w/api.php?action=help&modules=streamconfigs)
  async function getStreamTitle(streamName) {
    const API_REQUEST = 'https://meta.wikimedia.org/w/api.php?action=streamconfigs&format=json&formatversion=2&all_settings=1&constraints=destination_event_service%3Deventgate-analytics-external';
    const response = await axios.get(API_REQUEST)

    return (response.data.streams[streamName].schema_title);
  }

  function parseSampleRate(sampleRate) {
    let output = 'default: ' + sampleRate.default + '<br/>';
    if (sampleRate.custom) {
      sampleRate.custom.forEach((item) => {
        output += JSON.stringify(Array.from(item.wikis)).replace('[', '').replace(']', '').replaceAll('"', '') + ": " + item.rate + '<br/>';  
      });
    }

    return output;
  }

  function getKeyByValue(object, value) {
    for (let prop in object) {
        if (object.hasOwnProperty(prop)) {
            if (object[prop] === value)
                return prop;
        }
    }
  }

  // Split an error response into path and message
  function formatValidation (e) {
      return `${e.path}: ${e.msg}`;
  }

  // Format error response when saving/updating a instrument/AB test
  function formatError (data) {
      const title = `${data.title}`;
      const validation = `${(data.detail ?? []).map(formatValidation).join('\n')}`;
      return `${title}\n${validation}`;
  }

  function formatContextualAttributesOptions (data) {
      return data.map(e => e.contextual_attribute_name)
  }

  function formatContextualAttributeMap (data) {
      const contextualAttributesMap = [];
      data.forEach(element => {
          contextualAttributesMap[element.id] = element.contextual_attribute_name;
      });
      return contextualAttributesMap;
  }

  // sample_rate is stored like {"default": 0.1, "0.2": ["wiki", "wiki_3"], "0.3": ["wiki_2"]}
  // so shape that into what the forms need: {
  //   default: 0.1,
  //   custom: [
  //      { rate: 0.2, wikis: Array("wiki", "wiki_3") },
  //      { rate: 0.3, wikis: Array("wiki_2") }
  //   ]
  // }
  // TODO: enforce these schemas
  function transformSampleRateForUI (dbSampleRate) {
    return {
        'default': dbSampleRate['default'],
        custom: Object.keys(dbSampleRate)
            .filter(k => k !== 'default')
            .map(rateStr => ({
                rate: parseFloat(rateStr),
                wikis: dbSampleRate[rateStr],
                chipWikis: dbSampleRate[rateStr].map(item => ({label: item, value: item})),
            }))
    };
  }

  // basically the opposite of the transformation above
  function transformSampleRateForDB (uiSampleRate) {
    return Object.assign(
      { "default": uiSampleRate['default'] },
      uiSampleRate.custom.reduce((byRate, rateObj) => {
          const rate = rateObj.rate.toString();
          const wikis = byRate[rate] ?? [];
          byRate[rate] = Array.from(new Set(wikis.concat(rateObj.wikis)));
          return byRate;
      }, {})
    );
  }

  return {
    getSlugFormatted,
    getStreamTitle,
    splitStringAttributes,
    parseSampleRate,
    getKeyByValue,
    formatValidation,
    formatError,
    formatContextualAttributesOptions,
    formatContextualAttributeMap,
    transformSampleRateForUI,
    transformSampleRateForDB
  }
}

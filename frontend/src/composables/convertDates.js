import { UTCDate } from "@date-fns/utc";
import { format } from "date-fns";

export default function convertDates() {

  const getFormattedDate = (date) => {
   return format(new UTCDate(date), "yyyy-MM-dd");
  };

  function convertToSQLDateTime(jsDate) {
    return new UTCDate(jsDate);
  }

  return {
    getFormattedDate,
    convertToSQLDateTime
  };
}

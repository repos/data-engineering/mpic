import { defineStore } from 'pinia';
import { ref, computed } from 'vue';

// TODO a draft store to save user authentication state
export const useUserStore = defineStore('userAuth', {
  state: () => {
    const username = ref();
    const role = ref();

    const isLoggedIn = computed(() => Boolean(username.value));

    async function login(logUsername, logRole) {
      username.value = logUsername;
      role.value = logRole;
    }

    async function logout() {
      username.value = null;
      role.value = null;
    }

    return { username, role, isLoggedIn, login, logout };
  },
  persist: true
});

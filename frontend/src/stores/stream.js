import { defineStore } from 'pinia'
import { useUserStore } from './userAuth.js';
import axios from 'axios'

const baseUrl = import.meta.env.VITE_BASE_URL
let actionAPI = axios.create({
    timeout: 3000,
    baseURL: baseUrl
});

export const useStreamStore = defineStore('Stream', {
    state: () => ({
        stream: {
            name: '',
            schema_title: ''
        },
        streams: [],
        streamsMap: {}
    }),

    getters: {
        streamsMenu: (state) => (state.streams ?? []).map(stream => ({
            label: stream.name,
            description: stream.schema_title,
            value: stream.name,
        })),
    },

    actions: {
        async refreshStreams() {
            try {
                const { data } = await actionAPI.get('/streams');
                if (!(data instanceof Array)) {
                    throw 'Unexpected response ' + JSON.stringify(data);
                }
                this.streams = data;
                this.streamsMap = new Map(data.map((item) => [item.name, item.schema_title]));
            } catch (error) {
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                } else {
                    return {
                        data: error.response.data,
                        success: false,
                    };
                }
                console.error(error);
            }
        },
    },
});

import { defineStore } from 'pinia'
import axios from 'axios'
import convertDates from '../composables/convertDates.js'
import useUtility from '../composables/utility.js';
import { useUserStore } from './userAuth.js';
import { UTCDate } from '@date-fns/utc';
const { getFormattedDate, convertToSQLDateTime } = convertDates()
const { splitStringAttributes, getKeyByValue, formatError, formatContextualAttributesOptions, formatContextualAttributeMap,
        transformSampleRateForUI, transformSampleRateForDB } = useUtility();

const baseUrl = import.meta.env.VITE_BASE_URL
let mpicAPI = axios.create({
    timeout: 3000,
    baseURL: baseUrl,
});

function makeBlankInstrument () {
    return {
        name: '',
        slug: '',
        description: '',
        owner: [],
        purpose: [],
        task: '',
        type: 'instrument',
        utc_start_dt: '',
        utc_end_dt: '',
        compliance_requirements: '',
        sample_unit: '',
        sample_rate: {'default': 0.0},
        environments: '',
        // The client library will add agent_client_platform and agent_client_platform_family automatically.
        contextual_attributes: '',
        id: null,
        stream_name: '',
        schema_title: '',
        status: 0,
        was_activated: 0,
        // It's a instrument. There are no variants
        variants: null,
        acknowledgement: false
    };
}

function makeBlankExperiment () {
    return {
        name: '',
        slug: '',
        description: '',
        owner: [],
        purpose: [],
        task: '',
        type: 'A/B test',
        utc_start_dt: '',
        utc_end_dt: '',
        compliance_requirements: '',
        sample_unit: '',
        sample_rate: {'default': 0.0},
        environments: '',
        contextual_attributes: '',
        id: null,
        stream_name: '',
        schema_title: '',
        status: 0,
        // Empty default variant
        variants: [{
            name: '',
            type: '',
            values: ''
        }],
        acknowledgement: false
    };
}

// augment instrument/experiment
function augmentInstrument (data) {
    let instrumentData = Object.assign({}, data, {

        // TODO: this comes as a string, see if there's a way axios can decode it directly
        sample_rate: transformSampleRateForUI(data.sample_rate),
        // TODO not in the db?
        // instrument_type: 'instrument',
        // acknowledgement: null,
        security_legal_review: 'pending',
        status: data.status,
        was_activated: data.was_activated,
        utc_start_dt: data.utc_start_dt != '' ? getFormattedDate(data.utc_start_dt) : '',
        utc_end_dt: data.utc_start_dt != '' ? getFormattedDate(data.utc_end_dt) : '',
        contextual_attributes_bound: new Set(splitStringAttributes(data.contextual_attributes)),
        compliance_requirements_bound: new Set(splitStringAttributes(data.compliance_requirements)),
    });

    // If there are variants (it's a AB test) we add it to the instrument data
    if (data.variants !== null) {
        instrumentData =  Object.assign(instrumentData, {
            // FIXME Right now we are only considering experiments with one variant
            variants: [{
                name: data.variants[0].name,
                type: data.variants[0].type,
                values: data.variants[0].values.toString()
            }]
        });
    }

    return instrumentData;
}

// prepare data for saving instruments/AB tests
function prepareFormDataForSave (formData, map) {
    let contextualAttributesSelectedArray = (formData.contextual_attributes ?? '').split(',');
    // There is no selected contextual attributes. It's an A/B test. We explicitly empty the array to avoid undefined value later in the generated map
    if (contextualAttributesSelectedArray[0] === '') {
        contextualAttributesSelectedArray = [];
    }
    let key;
    const contextualAttributesObject = {};

    // contextual attributes are optional so let's check first if the user has selected any
    if (formData.contextual_attributes !== '') {
        for (const value of contextualAttributesSelectedArray) {
            key = getKeyByValue(map, value);
            contextualAttributesObject[key] = value;
        }
    }

    const withDefaults = Object.assign({
        contextual_attributes_map: Object.keys(contextualAttributesObject).length !== 0 ? contextualAttributesObject : {},
    }, formData);

    // Prepare variant values as an array in the case it's an A/B test
    if (formData.type === 'A/B test') {
        const singleVariant = formData.variants[0];
        if ((singleVariant.name === '') || (singleVariant.type === '') || (singleVariant === '')) {
            formData.variants = [];
        } else {
            if ((formData.variants[0].values) && (!Array.isArray(formData.variants[0].values))) {
                formData.variants[0].values = formData.variants[0].values.replace(' ', '').split(',');
            }
        }
    }

    formData.utc_start_dt = convertToSQLDateTime(formData.utc_start_dt);
    formData.utc_end_dt = convertToSQLDateTime(formData.utc_end_dt);

    return Object.assign(withDefaults, {
        sample_rate: JSON.stringify(transformSampleRateForDB(formData.sample_rate)),
        variants: formData.variants
    });
}

export const useInstrumentStore = defineStore('Instrument', {
    state: () => ({
        instrument: makeBlankInstrument(),
        instruments: [],
        formFunction: 'new',
        contextualAttributes: [],
        contextualAttributesMap: [],
        mappedContextualAttributes: []
    }),

    getters: {
        instrumentsMenu: (state) => (state.instruments ?? []).map(i => ({
            label: i.name,
            description: i.description,
            value: i.id,
            slug: i.slug
        })),
        isABTest: (state) => state.instrument.type === 'A/B test',
        isActivated: (state) => state.instrument.was_activated === 1,
        isActivatedABTest: (state) => state.isABTest && state.isActivated,
        isModifyFunction: (state) => state.formFunction === 'modify',
        isNewFunction: (state) => state.formFunction === 'new',
        // NOTE: v-tooltip is patched to not mount if it gets a non-string value, so returning "undefined" has purpose here
        activatedTooltip: (state) => state.isActivated ? `This field cannot be edited after the ${state.instrument.type} is activated` : undefined,
        activatedABTooltip: (state) => state.isABTest ? state.activatedTooltip : undefined,
        experimentFeatureControl: (state) => {
            const featureValuesArray = state.instrument.variants[0].values.split(',')
            return featureValuesArray[0]
        },
    },

    actions: {
        // Action to set the success message
        setSuccessMessage(message) {
            this.successMessage = message;
        },

        // Action to clear the success message
        clearSuccessMessage() {
            this.successMessage = null;
        },
        // refresh instruments and AB tests
        async refreshInstruments() {
            try {
                const { data } = await mpicAPI.get(`/instruments`);
                if (!(data instanceof Array)) throw 'Unexpected response ' + JSON.stringify(data);

                this.instruments = data;

                await this.setContextualAttributes()

            } catch (error) {
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                }
                console.error(error);
            }
        },
        // set an existing instrument
        async setInstrument(slug) {
            if (!slug) {
                this.instrument = augmentInstrument(makeBlankInstrument());
                return;
            }

            try {
                const { data } = await mpicAPI.get(`/instrument/${slug}`);

                if (!(data?.slug === slug)) throw 'Unexpected response ' + JSON.stringify(data);

                this.instrument = augmentInstrument(data);

            } catch (error) {
                console.error(error);
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                }
            }
        },
        // set an existing AB test
        async setExperiment(slug) {
            if (!slug) {
                this.instrument = augmentInstrument(makeBlankExperiment());
                return;
            }

            try {
                const { data } = await mpicAPI.get(`/experiment/${slug}`);

                if (!(data?.slug === slug)) throw 'Unexpected response ' + JSON.stringify(data);

                this.instrument = augmentInstrument(data);

            } catch (error) {
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                }
                console.error(error);
            }
        },
        // save a instrument/AB test
        async saveInstrument(rawFormData, type) {
            const formData = prepareFormDataForSave(rawFormData, this.contextualAttributesMap);
            const requestHeaders = {headers: {'Content-Type': 'application/json'}};
            let baseUrl;
            switch (type) {
                case 'launch-instrument':
                    baseUrl = 'instruments'
                    break
                case 'launch-experiment':
                    baseUrl = 'experiments'
                    break
                default:
                    throw `Unknown save type: ${type}`;
            }
            try {
                const response = await mpicAPI.post(baseUrl, formData, requestHeaders);
                return {
                    data: response.data,
                    success: true,
                };
            } catch (error) {
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                }
                // TODO: error.response.data.detail has validation errors.
                // These should be integrated with the forms via a validation library
                return {
                    data: {
                        title: formatError(error.response?.data ?? {})
                    },
                    success: false,
                };
            } finally {
                this.refreshInstruments();
            }
        },
        // Update a instrument/AB test
        async updateInstrument(rawFormData, type) {
            const formData = prepareFormDataForSave(rawFormData, this.contextualAttributesMap);
            const requestHeaders = {headers: {'Content-Type': 'application/json'}};
            let baseUrl;
            switch (type) {
                case 'update-instrument':
                    baseUrl = `instrument/${formData.slug}`
                    break
                case 'update-experiment':
                    baseUrl = `experiment/${formData.slug}`
                    break
                default:
                    throw `Unknown save type: ${type}`;
            }
            try {
                const response = await mpicAPI.put(baseUrl, formData, requestHeaders);
                return {
                    data: response.data,
                    success: true,
                };
            } catch (error) {
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                }
                // TODO: error.response.data.detail has validation errors.
                // These should be integrated with the forms via a validation library
                return {
                    data: {
                        title: formatError(error.response?.data ?? {})
                    },
                    success: false,
                };
            } finally {
                this.refreshInstruments();
            }
        },
        // Delete a instrument/AB test
        async deleteInstrument(data) {
            try {
                const response = await mpicAPI.delete(`instrument/${data.id}`);
                return {
                    data: response.data,
                    success: true,
                };
            } catch (error) {
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                }
                return {
                    data: {
                        title: error.response.data.title
                    },
                    success: false,
                };
            } finally {
                // FIXME if we reload the view we wouldn't need this
                // Besides, for some reason, it seems this is throwing an error
                //this.refreshInstruments();
            }
        },
        // activate/deactivate an instrument/AB test
        async patchInstrumentStatus(data) {
            try {
                const response = await mpicAPI.patch(`instrument/${data.id}`, data);
                return {
                    data: response.data,
                    success: true,
                };
            } catch (error) {
                return {
                    data: {
                        title: error.response.data.title
                    },
                    success: false,
                };
            } finally {
                this.refreshInstruments();
            }
        },
        async setContextualAttributes() {
            let requestUrl;
            if (['web', 'app'].includes(this.instrument.schema_type)) {
                requestUrl = `/contextual-attributes?schema-type=${this.instrument.schema_type}`;
            } else {
                requestUrl = `/contextual-attributes`;
            }
            try {
                const { data } = await mpicAPI.get(requestUrl);
                this.contextualAttributesMap = formatContextualAttributeMap(data);
                this.contextualAttributes = formatContextualAttributesOptions(data);
                this.mappedContextualAttributes.length = 0;
                this.mappedContextualAttributes.push(...this.contextualAttributes.map( item => ({ label: item, value: item}) ));
            } catch (error) {
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                }
                console.error(error);
            }
        }
    },
});

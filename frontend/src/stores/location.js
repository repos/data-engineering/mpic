import { defineStore } from 'pinia'
import { useUserStore } from './userAuth.js';
import axios from 'axios'

const baseUrl = import.meta.env.VITE_BASE_URL
let actionAPI = axios.create({
    timeout: 3000,
    baseURL: baseUrl
});

export const useLocationStore = defineStore('Location', {
    state: () => ({
        locations: []
    }),
    actions: {
        async refreshLocations() {
            try {
                const { data } = await actionAPI.get('/locations');
                if (!(data instanceof Array)) {
                    throw 'Unexpected response ' + JSON.stringify(data);
                }
                this.locations = data;
            } catch (error) {
                // If the response is 401 that means the user session has expired
                if (error.response.data.status === 401) {
                    const user = useUserStore();
                    user.logout();
                } else {
                    return {
                        data: error.response.data,
                        success: false,
                    };
                }
                console.error(error);
            }
        },
    },
});

import { createWebHistory, createRouter } from 'vue-router';
import { useInstrumentStore } from './stores/instrument.js';
import { useUserStore } from './stores/userAuth.js';

import List from './components/List.vue';
import Template from './components/Template.vue';
import Read from './components/Read.vue';
import LaunchInstrument from './components/LaunchInstrument.vue';
import LaunchExperiment from './components/LaunchExperiment.vue';
import UpdateInstrument from './components/UpdateInstrument.vue';
import UpdateExperiment from './components/UpdateExperiment.vue';

const routes = [
    { path: '/', name: 'catalog', component: List, alias: ['/list', '/catalog'] },
    { path: '/create-instrument', name: 'create-instrument', component: LaunchInstrument },
    { path: '/create-experiment', name: 'create-experiment', component: LaunchExperiment },
    { path: '/update-experiment/:slug?', name: 'update-experiment', component: UpdateExperiment, alias: ['/update-experiment', '/modify-experiment/'] },
    { path: '/update-instrument/:slug?', name: 'update-instrument', component: UpdateInstrument, alias: ['/update-instrument', '/modify-instrument/'] },
    { path: '/template/:slug?', name: 'template', component: Template, alias: ['/template', '/template/'] },
    { path: '/read/:slug', name: 'read', component: Read }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach(async (to) => {
    const user = useUserStore();

    /* if (!user.isLoggedIn) {
        router.push('/')
        return
    } */
    const pathsWithSlugs = ['update-experiment', 'update-instrument', 'template'];
    const instrumentStore = useInstrumentStore();
    if (pathsWithSlugs.includes(to.name)) {
        await instrumentStore.setInstrument(to.params.slug);
    }
    await instrumentStore.refreshInstruments();

});

export default router;

import './assets/main.css';
import './assets/form.css';
import '@wikimedia/codex/dist/codex.style.css';

import { createApp } from 'vue';
import router from './router';
import App from './App.vue';
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';
import { CdxTooltip } from '@wikimedia/codex';

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

const originalMounted = CdxTooltip.mounted;
const customizedTooltips = Object.assign(CdxTooltip, {
    mounted: (el, {value, arg}) => {
        // only call mount if passing a string, so we can pass `undefined` and hide
        if (typeof value === 'string') {
            originalMounted(el, {value, arg});
        }
    }
});

createApp(App)
    .use(router)
    .use(pinia)
    .directive( 'tooltip', customizedTooltips)
    .mount('#app');

import { $ } from '@wdio/globals'
import Page from './page.js';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ListPage extends Page {
    /**
     * define selectors using getter methods
     */
    get firstInstrumentName () {
        return $('table tr:first-child td:first-child')
    }

    get lastInstrumentName () {
        return $('table tr:last-child td:first-child')
    }

    get penultimateInstrumentName () {
        return $('table tr:last-child td::nth-last-child(2)')
    }

    get lastInstrumentActionMenu () {
        return $('table tr:last-child td:last-child')
    }

    get dialogDelete () {
        return $('div[data-id="dialog-delete"]');
    }

    get dialogDeleteButton () {
        return $('footer button:first-child');
    }

    async delete () {
        await this.lastInstrumentActionMenu.click()
        await this.lastInstrumentActionMenu.$('ul.cdx-menu__listbox li:last-child').click()
        await this.dialogDeleteButton.click()
    }

    async edit () {
        await this.firstInstrumentName.click()
        await this.firstInstrumentName.$('ul.cdx-menu__listbox li:first-child').click()
    }


    /**
     * overwrite specific options to adapt it to page object
     */
        open () {
            return super.open('');
        }
}

export default new ListPage();

import { $ } from '@wdio/globals'
import Page from './page.js';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ReadPage extends Page {
    /**
     * define selectors using getter methods
     */
    get description () {
        return $('div[data-id="description"]')
    }

    get task () {
        return $('div[data-id="task"]')
    }

    get email () {
        return $('div[data-id="email"]')
    }

    get contextualAttributes () {
        return $('div[data-id="contextual-attributes"]')
    }

    get sampleUnit () {
        return $('div[data-id="sample-unit"]')
    }

    get securityLegalReview () {
        return $('div[data-id="security-legal-review"]')
    }

    get complianceRequirements () {
        return $('div[data-id="compliance-requirements"]')
    }

    /**
     * overwrite specific options to adapt it to page object
     */
        open () {
            return super.open('read/test-instrument');
        }
}

export default new ReadPage();

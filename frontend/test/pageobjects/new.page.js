import { $ } from '@wdio/globals'
import Page from './page.js';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class NewPage extends Page {
    /**
     * define selectors using getter methods
     */
    get inputInstrumentName () {
        return $('input[data-id="instrument-name-new"]')
    }

    get inputDescription () {
        return $('textarea[data-id="description"]')
    }

    get inputSchemaType () {
        return $('input[name="radio-group-schema-type"][value="web"]')
    }

    get inputPhabTicket () {
        return $('input[data-id="task"]')
    }

    get inputOwner () {
        return $('div[data-id="owner"]')
    }

    get inputEmail () {
        return $('input[data-id="email"]')
    }

    get inputPurpose () {
        return $('div[data-id="purpose"]')
    }

    get inputContextualAttributes () {
        return $('section[data-id="contextual-attributes"] input')
    }

    get inputSampleUnit () {
        return $('div[data-id="sample-unit"]')
    }

    get inputDurationAmount () {
        return $('input[data-id="duration-amount"]')
    }

    get inputStartDate () {
        return $('input[data-id="start-date"]')
    }

    get inputSecurityLegalReview () {
        return $('input[data-id="security-legal-review"]')
    }

    get inputComplianceRequirements () {
        return $('section[data-id="compliance-requirements"] input')
    }

    get btnSubmit () {
        return $('button[data-id="launch-submit"]')
    }

    async fill (instrumentname, description, task, email, contextualattributes, durationamount, startdate, review, compliance) {
        await this.inputInstrumentName.setValue(instrumentname);
        await this.inputDescription.setValue(description);
        await this.inputSchemaType.click();
        await this.inputPhabTicket.setValue(task)

        // Select the first option in the team dropdown
        await this.inputOwner.click();
        const nextOwner = this.inputOwner.nextElement()
        await nextOwner.$('ul.cdx-menu__listbox li:first-child').click();

        await this.inputEmail.setValue(email)

        // Select the first option in the purpose dropdown
        await this.inputPurpose.click();
        const nextPurpose = this.inputPurpose.nextElement()
        await nextPurpose.$('ul.cdx-menu__listbox li:first-child').click()

        // Select chip input for contextual attributes
        await this.inputContextualAttributes.setValue(contextualattributes);
        const nextContextual = $('section[data-id="contextual-attributes"] div.cdx-text-input').nextElement()
        await nextContextual.$('ul.cdx-menu__listbox li').click()

        // Select the first option in the sample unit dropdown
        await this.inputSampleUnit.click();
        const nextSampleUnit = this.inputSampleUnit.nextElement()
        await nextSampleUnit.$('ul.cdx-menu__listbox li:first-child').click()

        await this.inputDurationAmount.setValue(durationamount);
        await this.inputStartDate.setValue(startdate);
        await this.inputSecurityLegalReview.setValue(review);

        // Select chip input for compliance requirements
        await this.inputComplianceRequirements.setValue(compliance);
        const nextCompliance = $('section[data-id="compliance-requirements"] div.cdx-text-input').nextElement()
        await nextCompliance.$('ul.cdx-menu__listbox li').click()
    }

    async submit () {
        await this.btnSubmit.click();
    }

    /**
     * overwrite specific options to adapt it to page object
     */
        open () {
            return super.open('launch');
        }
}

export default new NewPage();

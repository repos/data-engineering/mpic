import { browser, expect } from '@wdio/globals'
import ListPage from '../pageobjects/list.page.js'
import NewPage from '../pageobjects/new.page.js'
import ModifyPage from '../pageobjects/modify.page.js'
import ReadPage from '../pageobjects/read.page.js'

describe('MPIC application', () => {
    it('should see Catalog view with instruments', async () => {
        await ListPage.open()
        await expect(ListPage.firstInstrumentName).toBeExisting()
        await expect(ListPage.firstInstrumentName).toHaveText(
            'test Web Scroll UI')
        await expect(ListPage.lastInstrumentName).toBeExisting()
        await expect(ListPage.lastInstrumentName).toHaveText(
            'test Android Article TOC')
    })

    it('should submit new instrument', async () => {
        await NewPage.open()
        await NewPage.fill(
            'test instrument',
            'test description',
            'https://phabricator.wikimedia.org/T369544',
            'info@mwf.org',
            'page_id',
            8,
            '10-08-2024',
            'pending',
            'gdpr'
        )
       
        await NewPage.submit()

        await ListPage.open()
        await expect(ListPage.lastInstrumentName).toHaveText(
            'test instrument')
    })

    it('should submit modified instrument', async () => {
        await ModifyPage.open()
        await ModifyPage.fill(
            'test description modified',
            'https://phabricator.wikimedia.org/T369963',
            'data@mwf.org',
            'performer_id',
            10,
            '12-08-2024',
            'approved',
            'legal'
        )
       
        await ModifyPage.submit()

        await ReadPage.open()
        await expect(ReadPage.description).toHaveText('test description modified')
        await expect(ReadPage.task).toHaveText('T369963')
        await expect(ReadPage.email).toHaveText('data@mwf.org')
        await expect(ReadPage.contextualAttributes).toHaveText('page_id,performer_id')
        await expect(ReadPage.sampleUnit).toHaveText('pageview')
//        await expect(ReadPage.securityLegalReview).toHaveText('approved')
        await expect(ReadPage.complianceRequirements).toHaveText('legal,gdpr')        
    })

    it('should delete instrument', async () => {
        await ListPage.open()
        await expect(ListPage.lastInstrumentName).toHaveText(
            'test instrument')
        await ListPage.delete()
        await expect(ListPage.lastInstrumentName).toHaveText(expect.stringContaining('TOC'), { ignoreCase: true })
    })
})


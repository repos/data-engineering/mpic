'use strict';

const yaml = require('js-yaml');
const fs = require('fs');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const pino = require('pino');

// Set 'development' as the default environment if not other is provided
if (process.env.NODE_ENV === undefined) {
	process.env.NODE_ENV = 'development';
}
// Set default timezone
process.env.TZ = 'UTC';

// Load configuration properties
let configFile = 'config.dev.yaml';
const argv = yargs(hideBin(process.argv)).argv;
if (argv.config !== undefined) {
	configFile = argv.config;
}
const config = yaml.load(fs.readFileSync(configFile, 'utf8'));

// Logger configuration
const logger = pino({
    level: config.logging.level,
    formatters: {
        level: (label) => {
            return { level: label.toUpperCase() };
        }
    },
    timestamp: pino.stdTimeFunctions.isoTime,
	// Disable logging while running tests
	enabled: process.env.NODE_ENV !== 'test'
});

// Load secrets from enviroment variables
if (process.env.DATABASE_PASSWORD) {
	config.database.password = process.env.DATABASE_PASSWORD;
} else {
	logger.error('Error while setting an env variable. There is no value for DATABASE_PASSWORD');
}
if (process.env.SAL_PASSWORD) {
	config.sal.password = process.env.SAL_PASSWORD;
} else {
	logger.error('Error while setting an env variable. There is no value for SAL_PASSWORD');
}
if (process.env.IDP_CLIENT_SECRET) {
	config.idp.client_secret = process.env.IDP_CLIENT_SECRET;
} else {
	logger.error('Error while setting an env variable. There is no value for IDP_CLIENT_SECRET');
}
if (process.env.SESSION_SECRET) {
	config.service.session_secret = process.env.SESSION_SECRET;
} else {
	logger.error('Error while setting an env variable. There is no value for SESSION_SECRET');
}
if (process.env.CSRF_SECRET) {
	config.service.csrf_secret = process.env.CSRF_SECRET;
} else {
	logger.error('Error while setting an env variable. There is no value for CSRF_SECRET');
}

module.exports = {
    config,
    logger
};

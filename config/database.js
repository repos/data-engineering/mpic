'use strict';

const { config, logger } = require('./configuration');

const dbConfig = {
	host: config.database.host,
	user: config.database.username,
	password: config.database.password,
	database: config.database.database
};

const db = require('knex')({
	client: config.database.engine,
	connection: dbConfig
});

logger.info('Ready to connect to the database at ' + dbConfig.host + ':' + dbConfig.database);

exports.db = db;

'use strict';
const promClient = require('prom-client');


// Create a histogram for tracking HTTP request duration in seconds
const httpRequestDurationSeconds = new promClient.Histogram({
    name: 'http_request_duration_seconds',
    help: 'Duration of HTTP requests in seconds',
    labelNames: ['method', 'path', 'code'],
});

// Create a counter to track total HTTP requests
const httpRequestsTotal = new promClient.Counter({
    name: 'http_requests_total',
    help: 'How many HTTP requests processed, partitioned by status code, method, and HTTP path',
    labelNames: ['method', 'path', 'code'],
});

// Create a gauge to track in-flight HTTP requests
const inFlightRequests = new promClient.Gauge({
    name: 'http_requests_in_flight',
    help: 'Current number of in-flight HTTP requests',
});

module.exports = {
    httpRequestDurationSeconds,
    httpRequestsTotal,
    inFlightRequests,
};
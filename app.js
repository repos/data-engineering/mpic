'use strict';

const { config, logger } = require('./config/configuration');
const { httpRequestDurationSeconds, httpRequestsTotal, inFlightRequests } = require('./config/metrics'); // Use direct imports

const promClient = require('prom-client');
const instruments = require('./routes/instruments');
const experiments = require('./routes/experiments');
const login = require('./routes/login');
const streams = require('./routes/streams');
const locations = require('./routes/locations');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const history = require('connect-history-api-fallback');
const passport = require('passport');
const session = require('express-session');
const MemoryStore = require('memorystore')(session);
const helmet = require('helmet');

const app = express();
app.use(cors());
app.use(bodyParser.json({ limit: config.service.max_body_size || '100kb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

const historyMiddleware = history();
// Paths that must be ignore by the history-api-fallback middleware
const historyMiddlewareIgnoreList = [
    '/login', 
    '/login/callback', 
    '/api/v1/instruments',
    '/api/v1/experiments'
];

promClient.collectDefaultMetrics();

// Middleware to measure response time and count HTTP requests
app.use((req, res, next) => {
    // Start tracking in-flight requests
    inFlightRequests.inc();

    // Start the timer for the duration histogram
    const end = httpRequestDurationSeconds.startTimer();

    const method = req.method;
    const path = req.route ? req.route.path : req.path; 

    // When the response finishes, record metrics
    res.on('finish', () => {
        const statusCode = res.statusCode;

        // Stop the timer and capture the duration of the request
        end({ method, path, code: statusCode }); 
        // Increment the counter for total HTTP requests
        httpRequestsTotal.inc({
            code: statusCode,
            method: method.toLowerCase(),
            path: path // Use path instead of route
        });

        // Decrement the in-flight requests gauge
        inFlightRequests.dec();
    });

    next();
});


// Expose the metrics at the /metrics endpoint
app.get('/metrics', async (req, res) => {
    try {
        res.set('Content-Type', promClient.register.contentType);
        res.end(await promClient.register.metrics());
    } catch (ex) {
        res.status(500).end(ex);
    }
});

app.use(function (req, res, next) {
    req.app = {};
    req.app.conf = config;
    req.app.logger = logger;

    // Log every request in detail
    req.app.logger.debug(req);

    // historyMiddleware must ignore some paths to be managed directly by the backend
    if (historyMiddlewareIgnoreList.includes(req.path)) {
        next();
    } else {
        historyMiddleware(req, res, next);
    }
});

app.use(express.static(__dirname + '/public'));

app.set('trust proxy', 1);
if (process.env.NODE_ENV === 'production') {
    app.use(session({
        secret: config.service.session_secret,
        resave: true,
        proxy: true,
        saveUninitialized: false,
        name: 'mpic',
        store: new MemoryStore({
            checkPeriod: 86400000 // prune expired entries every 24h
        }),
        cookie: {
            httpOnly: true,
            secure: config.service.secure_cookie,
            sameSite: 'None',
            maxAge: 600000
        }
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(helmet());
}

app.use('/', instruments);
app.use('/', experiments);
app.use('/', login);
app.use('/', streams);
app.use('/', locations);

module.exports = {
    logger,
    config,
    app
};

'use strict';

const MWBot = require('mwbot');

function getActionLoggerEntry(creator, instrumentName, action) {
	const now = new Date();
	const msgStart = action === 'created' ? 'A new' : 'An';
	return '\n* ' + now.getHours().toString().padStart(2, '0') + ':' + now.getMinutes().toString().padStart(2, '0') +
	' ' + creator + ': ' + msgStart + ' instrument has been ' + action + ' - ' + instrumentName;
}

function getCurrentSalHeader() {
	const now = new Date();
	return '\n===' + now.toISOString().split('T')[0] + '===\n';
}

function getPageContent(response) {
	let content = response.query.pages;
	content = content[Object.keys(content)[0]];
	return content.revisions[0].slots.main['*'];
}

function logAction(req, instrumentName, creator, action) {
	const bot = new MWBot();
	bot.loginGetEditToken({
		apiUrl: req.app.conf.sal.api_url,
		username: req.app.conf.sal.username,
		password: req.app.conf.sal.password
	}).then(() => {
		return bot.read(req.app.conf.sal.page);
	}).then((response) => {
		// Get current SAL content
		let pageContent = getPageContent(response);
		// Prepare the new SAL entry
		const currentDayHeader = getCurrentSalHeader();
		if (!pageContent.includes(currentDayHeader)) {
			pageContent += currentDayHeader;
		}
		// Log the new entry to the SAL
		pageContent += getActionLoggerEntry(creator, instrumentName, action);
		return bot.update(req.app.conf.sal.page, pageContent);
	}).then((response) => {
		req.app.logger.info('Added an entry to SAL');
	}).catch((error) => {
		req.app.logger.error('An error has found while logging to SAL: ' + error);
	});
}

module.exports = {
	getActionLoggerEntry,
	logAction
};

'use strict';

// Calculate the percentage of progress between two dates
function getProgress(startDate, endDate) {
	const start = new Date(startDate).getTime();
	const end = new Date(endDate).getTime();
	const now = new Date(Date.now()).getTime();

	if (now < start) {
		return 0;
	}
	if (now > end) {
		return 1;
	}

	const total = end - start;
	const currentDays = now - start;
	const progress = Math.round(currentDays / total * 100) / 100;

	return progress;
}

module.exports = {
	getProgress
};

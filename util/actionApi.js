'use strict';

const { config } = require('../config/configuration');
const axios = require('axios');

// Get stream configuration from Action API to return an array
// with all the stream names and schema titles
async function findStreams() {
    const API_REQUEST = config.listeners.action_api_basepath +
        '/w/api.php?action=streamconfigs&format=json&formatversion=2&all_settings=1' +
        '&constraints=destination_event_service%3Deventgate-analytics-external';
    const response = await axios.get(API_REQUEST, {
        headers: {
            Host: 'meta.wikimedia.org'
        }
    });

    return parseStreams(response.data);
}

function parseStreams(data) {
    const rawStreams = data.streams;

    const streams = [];
    for (const streamName in rawStreams) {
        const newStream = {
            name: streamName,
            schema_title: rawStreams[streamName].schema_title
        };
        streams.push(newStream);
    }

    return streams;
}

// TODO
async function findLocations() {
    const API_REQUEST = config.listeners.action_api_basepath + '/w/api.php?action=sitematrix&smsiteprop=dbname&smlangprop=site&format=json';
    const response = await axios.get(API_REQUEST, {
        headers: {
            Host: 'meta.wikimedia.org'
        }
    });

    return parseLocations(response.data);
}

function parseLocations(data) {
    const sitematrix = data.sitematrix;

    const locations = [];
    for (const [key, value] of Object.entries(sitematrix)) {
        // Some items are classified as 'specials'
        if (key === 'specials') {
            value.forEach((item) => {
                locations.push(item.dbname);
            });
        }

        // Some items can be empty
        if (value.site === undefined) {
            continue;
        }

        // The rest of the sites
        value.site.forEach((item) => {
            locations.push(item.dbname);
        });
    }

    return locations;
}

module.exports = {
	findStreams,
    parseStreams,
    findLocations,
    parseLocations
};

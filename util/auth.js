'use strict';

const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const { config } = require('../config/configuration');

// Check if a user is authenticated or not
const isAuth = function (req, res, next) {
    // FIXME Login bypass
    if ((process.env.NODE_ENV === 'development') || (process.env.NODE_ENV === 'test')) {
        req.user = {
			id: 'testuser'
		};
        return next();
    }

    if (req.user) {
        return next();
    } else {
        res.status(401).send({
            status: 401,
            type: 'unauthorized',
            title: 'Unauthorized'
        });
        req.app.logger.info('unauthorized user');
        res.end();
    }

};

// Get the common name from a LDAP group definition
const getCommonName = function (group) {
    return group.split(',')[0].split('=')[1];
};

// Get user groups from 'memberOf' LDAP Attribute
const getUserGroups = function (memberOf) {
    const groups = [];
    if (Array.isArray(memberOf)) {
        memberOf.forEach(function (item, index, array) {
            groups.push(getCommonName(item));
        });
    } else {
        groups.push(getCommonName(memberOf));
    }

    return groups;
};

// Generate and get a new CSRF Token
// Based on https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html#employing-hmac-csrf-tokens
const getCsrfToken = function (req) {
    const randomValue = uuidv4();
    const message = req.session.id + '!' + randomValue;
    const csrfToken = crypto.createHmac('sha256', config.service.csrf_secret)
                        .update(message)
                        .digest('hex');
    return csrfToken;
};

// Check and validate the CRSF token
const validateCsrfToken = function (req, res, next) {
    // FIXME Login bypass
    if (process.env.LOGIN_BYPASS !== '') {
        return next();
    }

    if (!req.user || !req.body.csrfToken || req.body.csrfToken !== req.session.csrfToken) {
        res.status(401).send({
            status: 401,
            type: 'unauthorized',
            title: 'Unauthorized'
        });
        req.app.logger.info('invalid or inexistent CSRF token');
        res.end();
    }
};

exports.isAuth = isAuth;
exports.getUserGroups = getUserGroups;
exports.getCsrfToken = getCsrfToken;
exports.validateCsrfToken = validateCsrfToken;
